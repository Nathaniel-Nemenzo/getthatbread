import unittest
import requests

import db.models
import db.schemas

API_HOST = "https://api.getthatbread.me"

class FlaskTest(unittest.TestCase):
    def test_job_model(self):
        job = db.models.Job.query.get('97de0985e2c7a210')
        d = job.__dict__
        self.assertEqual(d['companyId'], 'Amazon.com')

    def test_city_model(self):
        city = db.models.City.query.get('New York City, NY')
        d = city.__dict__
        self.assertEqual(d['walk_score_url'], 'https://www.walkscore.com/NY/New_York')

    def test_company_model(self):
        company = db.models.Company.query.get('Amazon.com')
        d = company.__dict__
        self.assertEqual(d['companyName'], 'Amazon.com')

    def test_job_schema(self):
        job = db.models.Job.query.get('97de0985e2c7a210')

        # instead of internal dict, go thru the schema
        d = db.schemas.job_schema.dump(job)
        self.assertEqual(d['companyId'], 'Amazon.com')
    
    def test_company_schema(self):
        company = db.models.Company.query.get('Amazon.com')
        d = db.schemas.company_schema.dump(company)
        self.assertEqual(d['companyName'], 'Amazon.com')

    def test_city_schema(self):
        city = db.models.City.query.get('New York City, NY')
        
        d = db.schemas.city_schema.dump(city)
        self.assertEqual(d['walk_score_url'], 'https://www.walkscore.com/NY/New_York')

    def test_jobs_schema(self):
        all_jobs = db.models.db.session.query(db.models.Job)
        result = db.schemas.jobs_schema.dump(all_jobs)
        self.assertEqual(len(result), 394)

    def test_companies_schema(self):
        all_companies = db.models.db.session.query(db.models.Company)
        result = db.schemas.companies_schema.dump(all_companies)
        self.assertEqual(len(result), 288)
    
    def test_cities_schema(self):
        all_cities = db.models.db.session.query(db.models.City)
        result = db.schemas.cities_schema.dump(all_cities)
        self.assertEqual(len(result), 102)

    def test_jobs_status(self):
        response = requests.get(API_HOST + "/api/jobs")
        self.assertEqual(response.status_code, 200)

    def test_jobs_content(self):
        response = requests.get(API_HOST + "/api/jobs?limit=2&&offset=1")
        self.assertEqual(response.json()["count"], 2)

    def test_jobs_content_keys(self):
        response = requests.get(API_HOST + "/api/jobs")
        response = response.json()
        self.assertEqual(
            set(response["data"][0]),
            {
                "jobId",
                "jobTitle",
                "jobType",
                "creationDate",
                "salary",
                "companyName",
                "companyId",
                "cityId",
                "location",
                'country',
                'salary_when',
                'salary_lower_bound',
                'locationZip',
                'indeedFinalUrl',
                'locality',
                'description',
                'salary_upper_bound'
            },
        )

    def test_jobs_search(self):
        response = requests.get(
            API_HOST
            + "/api/jobs?limit=2&&title=engineer&&company=Boeing&&location=Seattle"
        )
        self.assertEqual(response.status_code, 200)
        
    def test_jobs_query(self):
        response = requests.get(
            API_HOST
            + "/api/jobs?limit=2&&query=engineer"
        )
#        print("job query===" + str(response.json()))        
        self.assertEqual(response.status_code, 200)
        
    def test_job_status_code(self):
        response = requests.get(API_HOST + "/api/jobs/c6b33a8a755c628e")
        self.assertEqual(response.status_code, 200)

    def test_job_content_keys(self):
        response = requests.get(API_HOST + "/api/jobs/c6b33a8a755c628e")
        response = response.json()
        self.assertEqual(
            set(response),
            {
                "jobId",
                "jobTitle",
                "description",
                "jobType",
                "creationDate",
                "salary",
                "indeedFinalUrl",
                "companyName",
                "companyId",
                "cityId",
                "country",
                "locality",
                "location",
                "locationZip",
                'salary_lower_bound',
                'salary_upper_bound',
                'salary_when'
            },
        )

    def test_cities_status(self):
        response = requests.get(API_HOST + "/api/cities")
        self.assertEqual(response.status_code, 200)

    def test_cities_content(self):
        response = requests.get(API_HOST + "/api/cities?limit=2&&offset=1")
        self.assertEqual(response.json()["count"], 2)

    def test_cities_content_keys(self):
        response = requests.get(API_HOST + "/api/cities")
        response = response.json()
        self.assertEqual(
            set(response["data"][0]),
            {"cityId", "cityName", "state", "country", "population", "timezone", 'safety', 'geonames_id', 'covid', 'getyourguide_url', 'mentions', 'airbnb_url', 'known_for', 'latitude', 'highImage', 'walk_score_url', 'elevationMeters', 'mediumImage', 'countryCode', 'foursquare_url', 'defaultImage', 'kayak_car_rental_url', 'budget', 'stateCode', 'average_rating', 'longitude', 'photos'},
        )

    def test_cities_search(self):
        response = requests.get(
            API_HOST + "/api/cities?limit=1&&name=austin&&state=TX&&population=100000"
        )
        self.assertEqual(response.status_code, 200)
        
    def test_cities_query(self):
        response = requests.get(
            API_HOST + "/api/cities?limit=1&&query=austin"
        )
#        print("city query===" + str(response.json()))        
        self.assertEqual(response.status_code, 200)        

    def test_city_status_code(self):
        response = requests.get(API_HOST + "/api/cities/Austin, TX")
        self.assertEqual(response.status_code, 200)

    def test_city_content_keys(self):
        response = requests.get(API_HOST + "/api/cities/Austin, TX")
        response = response.json()
        self.assertEqual(
            set(response),
            {
                "cityId",
                "cityName",
                "state",
                "stateCode",
                "country",
                "countryCode",
                "elevationMeters",
                "latitude",
                "longitude",
                "population",
                "timezone",
                "defaultImage",
                "mediumImage",
                "highImage",
                'roadgoat_url',
                'destination_type',
                'geonames_id',
                'walk_score_url',
                'budget',
                'safety',
                'covid',
                'average_rating',
                'foursquare_url',
                'kayak_car_rental_url',
                'kayak_lodging_url',
                'airbnb_url',
                'getyourguide_url',
                'known_for',
                'photos',
                'mentions'
            },
        )

    def test_companies_status(self):
        response = requests.get(API_HOST + "/api/companies")
        self.assertEqual(response.status_code, 200)

    def test_companies_content(self):
        response = requests.get(API_HOST + "/api/companies?limit=2&&offset=1")
        self.assertEqual(response.json()["count"], 2)

    def test_companies_content_keys(self):
        response = requests.get(API_HOST + "/api/companies")
        response = response.json()
        self.assertEqual(
            set(response["data"][0]),
            {"companyId", 
            "companyName", 
            "employees", 
            "hqLocation", 
            "rating", 
            "sector",
            'facebookId',
            'logoUrl',
            'employee_upper_bound',
            'reviewCount',
            'twitterId',
            'indeedFinalUrl',
            'revenue',
            'description',
            'urlText',
            'url',
            'founded',
            'employee_lower_bound'},
        )

    def test_companies_search(self):
        response = requests.get(
            API_HOST
            + "/api/companies?limit=2&&hqLocation=Bentonville&&name=Walmart&&rating=3.0"
        )
        self.assertEqual(response.status_code, 200)
        
    def test_companies_query(self):
        response = requests.get(
            API_HOST
            + "/api/companies?limit=2&&query=Walmart"
        )
#        print("company query===" + str(response.json()))        
        self.assertEqual(response.status_code, 200)
        
    def test_company_status_code(self):
        response = requests.get(API_HOST + "/api/companies/Walmart")
        self.assertEqual(response.status_code, 200)

    def test_company_content_keys(self):
        response = requests.get(API_HOST + "/api/companies/Walmart")
        response = response.json()
        self.assertEqual(
            set(response),
            {
                "companyId",
                "companyName",
                "description",
                "employees",
                "founded",
                "hqLocation",
                "url",
                "urlText",
                "indeedFinalUrl",
                "logoUrl",
                "rating",
                "revenue",
                "reviewCount",
                "sector",
                "facebookId",
                "twitterId",
                'employee_upper_bound',
                'employee_lower_bound'
            },
        )


if __name__ == "__main__":
    unittest.main()
