from flask import jsonify, request

from db.models import app, db as database, City, Job, Company, KnownFor
from db.schemas import jobs_schema, cities_schema, known_for_schema, companies_schema


from db.helpers import object_as_dict, get_query

# FILTERING / SEARCHING / SORTING CODE CITATION: https://gitlab.com/forbesye/fitsbits/-/tree/master/back-end

@app.route("/")
def hello_world():
    return '<img src="https://i.ytimg.com/vi/QO68lvIVc-0/hqdefault.jpg" />'

@app.route("/api/jobs", methods=["GET"])
def get_jobs():
    import db.job
    queries = request.args.to_dict(flat = False)

    # get all jobs
    all_jobs = database.session.query(Job)

    # searching
    query = get_query('query', queries)
    if query:
        all_jobs = db.job.search_jobs(query, all_jobs)

    # filtering
    all_jobs = db.job.filter_jobs(queries, all_jobs)

    # sorting
    sort = get_query('sort', queries)
    if sort:
        all_jobs = db.job.sort_jobs(sort, all_jobs)

    # apply offset
    offset = get_query('offset', queries)
    if offset:
        all_jobs = all_jobs.offset(int(offset[0]))

    # apply limit
    limit = get_query('limit', queries)
    if limit:
        all_jobs = all_jobs.limit(int(limit[0])).all()

    result = jobs_schema.dump(all_jobs)
    return {"data": result, "count": len(result)}


@app.route("/api/jobs/<id>", methods=["GET"])
def get_job_detail(id):
    job = Job.query.get(id)
    d = object_as_dict(job)
    return jsonify(d)


@app.route("/api/cities", methods=["GET"])
def get_cities():
    import db.city
    queries = request.args.to_dict(flat = False)

    # get all cities
    all_cities = database.session.query(City)

    # searching
    query = get_query('query', queries)
    if query:
        all_cities = db.city.search_cities(query, all_cities)
    
    # filtering
    all_cities = db.city.filter_cities(queries, all_cities)

    # sorting
    sort = get_query('sort', queries)
    if sort:
        all_cities = db.city.sort_cities(sort, all_cities)

    # apply offset
    offset = get_query('offset', queries)
    if offset:
        all_cities = all_cities.offset(int(offset[0]))

    # apply limit
    limit = get_query('limit', queries)
    if limit:
        all_cities = all_cities.limit(int(limit[0])).all()

    result = cities_schema.dump(all_cities)
    return {"data": result, "count": len(result)}


@app.route("/api/cities/<id>", methods=["GET"])
def get_city_detail(id):
    city = City.query.get(id)
    d = object_as_dict(city)
    
    # parse out known for and put inside d
    known_for = d['known_for'].split(';')
    d['known_for'] = {'data': []}
    for id in known_for:
        if id.isnumeric():
            kf = KnownFor.query.get(id)
            k = known_for_schema.dump(kf)
            d['known_for']['data'].append(k)

    # parse out photos
    photos = d['photos'].split(';')
    d['photos'] = {'data': []}
    for photo in photos:
        d['photos']['data'].append(photo)
    return jsonify(d)


@app.route("/api/companies", methods=["GET"])
def get_companies():
    import db.company
    queries = request.args.to_dict(flat = False)

    # get all companies
    all_companies = database.session.query(Company)

    # searching
    query = get_query('query', queries)
    if query:
        all_companies = db.company.search_companies(query, all_companies)

    # filtering
    all_companies = db.company.filter_companies(queries, all_companies)

    # sorting
    sort = get_query('sort', queries)
    if sort:
        all_companies = db.company.sort_companies(sort, all_companies)

    # apply offset   
    offset = get_query('offset', queries)    
    if offset:
        all_companies = all_companies.offset(int(offset[0]))

    limit = get_query('limit', queries)
    if limit:
        all_companies = all_companies.limit(int(limit[0])).all()

    result = companies_schema.dump(all_companies)
    return {"data": result, "count": len(result)}


@app.route("/api/companies/<id>", methods=["GET"])
def get_company_detail(id):
    company = Company.query.get(id)
    d = object_as_dict(company)
    return jsonify(d)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
