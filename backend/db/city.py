'''
city.py: helper methods for interacting with cities in the database
'''


from .models import City
from sqlalchemy import or_
from .helpers import get_query

def search_cities(query, all):
    '''
    search_companies: iterates though terms and searches string fields
    '''
    if not query:
        return all
    query = query[0].strip()
    terms = query.split()
    terms = [w.lower() for w in terms]

    for term in terms:
        searches = []
        searches.append(City.cityName.ilike(f'%{ term }%'))
        searches.append(City.state.ilike(f'%{ term }%'))
        searches.append(City.stateCode.ilike(f'%{ term }%'))
        searches.append(City.country.ilike(f'%{ term }%'))
        searches.append(City.countryCode.ilike(f'%{ term }%'))

    all = all.filter(or_(*tuple(searches)))
    return all

def filter_city_by(query, filtering, what):
    what = what[0]
    if filtering == 'city_name':
        query = query.filter(City.cityName.ilike(f'%{ what }%'))
    elif filtering == 'country':
        query = query.filter(City.country.ilike(f'%{ what }%'))
    elif filtering == 'elevation':
        query = query.filter(City.elevationMeters >= int(what))
    elif filtering == 'budget':
        query = query.filter(City.budget > int(what))
    elif filtering == 'safety':
        query = query.filter(City.safety > int(what))
    elif filtering == 'covid':
        query = query.filter(City.covid < float(what))
    elif filtering == 'average_rating':
        query = query.filter(City.average_rating > float(what))
    elif filtering == 'state':
        query = query.filter(City.state.ilike(f'%{ what }%'))
    elif filtering == 'population':
        query = query.filter(City.population <= int(what))
    
    return query
 
def filter_cities(queries, all):
    city_name = get_query('name', queries) 
    country = get_query('country', queries) 
    elevation = get_query('elevation_meters', queries)  
    budget = get_query('budget', queries)  
    safety = get_query('safety', queries)  
    covid = get_query('covid', queries)  
    average_rating = get_query('average_rating', queries)  
    state = get_query('state', queries)
    population = get_query('population', queries)

    if city_name:
        all = filter_city_by(all, 'city_name', city_name)
    if population:
        all = filter_city_by(all, 'population', population)
    if state:
        all = filter_city_by(all, 'state', state)
    if country:
        all = filter_city_by(all, 'country', country)
    if elevation:
        all = filter_city_by(all, 'elevation', elevation)
    if budget:
        all = filter_city_by(all, 'budget', budget)
    if safety:
        all = filter_city_by(all, 'safety', safety)
    if covid:
        all = filter_city_by(all, 'covid', covid)
    if average_rating:
        all = filter_city_by(all, 'average_rating', average_rating)

    return all


def sort_city_by(sorting, query, desc):
    q = None
    if sorting == 'budget':
        q = City.budget
    elif sorting == 'safety':
        q = City.safety
    elif sorting == 'covid':
        q = City.covid
    elif sorting == 'average_rating':
        q = City.average_rating
    elif sorting == 'name':
        q = City.cityName
    else:
        return query

    if desc:
        return query.order_by(q.desc())
    else:
        return query.order_by(q)

def sort_cities(query, all):
    if not query:
        return all
    query = query[0].strip()
    if '^' in query:
        return sort_city_by(query.replace('^', ""), all, True)
    else:
        return sort_city_by(query.replace('^', ""), all, False)