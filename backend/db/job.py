'''
job.py: helper methods for interacting with jobs in the database
'''

from .models import Job
from .schemas import jobs_schema
from sqlalchemy import or_
from .helpers import get_query

def search_jobs(query, all):
    '''
    search_jobs: iterates though terms and searches string fields
    '''
    if not query:
        return all
    query = query[0].strip()
    terms = query.split()
    terms = [w.lower() for w in terms]
    for term in terms:
        searches = []
        searches.append(Job.jobTitle.ilike(f'%{ term }%'))
        searches.append(Job.description.ilike(f'%{ term }%'))
        searches.append(Job.jobType.ilike(f'%{ term }%'))
        searches.append(Job.companyName.ilike(f'%{ term }%'))
        searches.append(Job.location.ilike(f'%{ term }%'))
        searches.append(Job.locationZip.ilike(f'%{ term }%'))

    all = all.filter(or_(*tuple(searches)))
    return all

def filter_job_by(query, filtering, what):
    what = what[0]
    if filtering == 'job_title':
        query = query.filter(Job.jobTitle.ilike(f'%{ what }%'))
    elif filtering == 'company':
        query = query.filter(Job.companyName.ilike(f'%{ what }%'))
    elif filtering == 'location':
        query = query.filter(Job.location.ilike(f'%{ what }%'))
    elif filtering == 'job_type':
        query = query.filter(Job.jobType.ilike(f'%{ what }%'))
    elif filtering == 'salary':
        query = query.filter(Job.salary_lower_bound >= float(what))
    
    return query
 
def filter_jobs(queries, all):
    job_title = get_query('job_title', queries) 
    company = get_query('company', queries) 
    location = get_query('location', queries) 
    job_type = get_query('job_type', queries) 
    salary = get_query('salary', queries) 

    if job_title:
        all = filter_job_by(all, 'job_title', job_title)
    if company:
        all = filter_job_by(all, 'company', company)
    if location:
        all = filter_job_by(all, 'location', location)
    if job_type:
        all = filter_job_by(all, 'job_type', job_type)
    if salary:
        all = filter_job_by(all, 'salary', salary)

    return all


def sort_job_by(sorting, query, desc):
    q = None
    if sorting == 'country':
        q = Job.country
    elif sorting == 'salary':
        q = Job.salary_lower_bound
    elif sorting == 'salary_upper_bound':
        q = Job.salary_upper_bound
    elif sorting == 'company':
        q = Job.companyName
    elif sorting == 'job_title':
        q = Job.jobTitle
    else:
        return query

    if desc:
        return query.order_by(q.desc())
    else:
        return query.order_by(q)

def sort_jobs(query, all):
    if not query:
        return all
    query = query[0].strip()
    if '^' in query:
        return sort_job_by(query.replace('^', ""), all, True)
    else:
        return sort_job_by(query.replace('^', ""), all, False)