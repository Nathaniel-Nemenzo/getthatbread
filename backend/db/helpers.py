from sqlalchemy import inspect

def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}

def get_query(key, queries):
    try:
        return queries[key]
    except KeyError:
        return None