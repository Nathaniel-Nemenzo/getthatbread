'''
company.py: helper methods for interacting with companies in the database
'''

from .models import Company
from sqlalchemy import or_
from .helpers import get_query

def search_companies(query, all):
    '''
    search_companies: iterates though terms and searches string fields
    '''
    if not query:
        return all
    query = query[0].strip()
    terms = query.split()
    terms = [w.lower() for w in terms]

    for term in terms:
        searches = []
        searches.append(Company.companyName.ilike(f'%{ term }%'))
        searches.append(Company.description.ilike(f'%{ term }%'))
        searches.append(Company.hqLocation.ilike(f'%{ term }%'))
        searches.append(Company.sector.ilike(f'%{ term }%'))

    all = all.filter(or_(*tuple(searches)))
    return all

def filter_company_by(query, filtering, what):
    what = what[0]
    # TODO: ADD MORE FILTER
    if filtering == 'company_name':
        query = query.filter(Company.companyName.ilike(f'%{ what }%'))
    elif filtering == 'hq_location':
        query = query.filter(Company.hqLocation.ilike(f'%{ what }%'))
    elif filtering == 'sector':
        query = query.filter(Company.sector.ilike(f'%{ what }%'))
    elif filtering == 'employees_lower':
        query = query.filter(Company.employee_lower_bound < int(what))
    elif filtering == 'rating':
        query = query.filter(Company.rating >= float(what))
    elif filtering == 'review_count':
        query = query.filter(Company.reviewCount >= int(what))
    
    return query
 
def filter_companies(queries, all):
    company_name = get_query('name', queries) 
    hq_location = get_query('hq_location', queries) 
    sector = get_query('sector', queries)
    employees_lower = get_query('employees', queries)
    rating = get_query('rating', queries)
    review_count = get_query('review_count', queries)

    if company_name:
        all = filter_company_by(all, 'company_name', company_name)
    if hq_location:
        all = filter_company_by(all, 'hq_location', hq_location)
    if sector:
        all = filter_company_by(all, 'sector', sector)
    if employees_lower:
        all = filter_company_by(all, 'employees_lower', employees_lower)
    if rating:
        all = filter_company_by(all, 'rating', rating)
    if review_count:
        all = filter_company_by(all, 'review_count')

    return all


def sort_company_by(sorting, query, desc):
    q = None
    if sorting == 'name':
        q = Company.companyName
    elif sorting == 'hq_location':
        q = Company.hqLocation
    elif sorting == 'sector':
        q = Company.sector
    elif sorting == 'employees':
        q = Company.employee_lower_bound
    elif sorting == 'rating':
        q = Company.rating
    elif sorting == 'review_count':
        q = Company.reviewCount
    else:
        return query

    if desc:
        return query.order_by(q.desc())
    else:
        return query.order_by(q)

def sort_companies(query, all):
    if not query:
        return all
    query = query[0].strip()
    if '^' in query:
        return sort_company_by(query.replace('^', ""), all, True)
    else:
        return sort_company_by(query.replace('^', ""), all, False)