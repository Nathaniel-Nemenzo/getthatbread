import sys

from models import db, City, Job, Company
import pandas as pd

job_list = []
city_list = []
company_list = []

def add_jobs():
    job_table = {}
    df = pd.read_csv(r"data/jobs_updated.csv")
    print(df)
    for ind in df.index:
        jobId = str(df["id"][ind])
        if not jobId in job_table:
            jobTitle = str(df["title"][ind])
            description = str(df["description"][ind])
            jobType = str(df["job_type"][ind])
            creationDate = str(df["creation_date"][ind])
            salary = str(df["salary"][ind])
            indeedFinalUrl = str(df["indeed_final_url"][ind])
            companyName = str(df["company_name"][ind])
            companyId = str(df["companyId"][ind])
            cityId = str(df["cityId"][ind])
            country = str(df["country"][ind])
            locality = str(df["locality"][ind])
            location = str(df["location"][ind])
            locationZip = str(df["zip"][ind])
            salary_lower_bound = float(df['salary_lower_bound'][ind])
            salary_upper_bound = float(df['salary_upper_bound'][ind])
            salary_when = str(df['salary_when'][ind])


            job = Job(
                jobId=jobId,
                jobTitle=jobTitle,
                description=description,
                jobType=jobType,
                creationDate=creationDate,
                salary=salary,
                indeedFinalUrl=indeedFinalUrl,
                companyName=companyName,
                companyId=companyId,
                cityId=cityId,
                country=country,
                locality=locality,
                location=location,
                locationZip=locationZip,
                salary_lower_bound = salary_lower_bound,
                salary_upper_bound = salary_upper_bound,
                salary_when = salary_when
            )
            job_list.append(job)
            job_table[jobId] = "y"


def add_cities():
    df = pd.read_csv(r"data/cities_updated.csv")
    print(df)
    for ind in df.index:
        cityId = str(df["cityId"][ind])
        cityName = str(df["name"][ind])
        state = str(df["region"][ind])
        stateCode = str(df["regionCode"][ind])
        country = str(df["country"][ind])
        countryCode = str(df["countryCode"][ind])
        elevationMeters = int(df["elevationMeters"][ind])
        latitude = float(df["latitude"][ind])
        longitude = float(df["longitude"][ind])
        population = int(df["population"][ind])
        timezone = str(df["timezone"][ind])
        defaultImage = str(df["defaultImage"][ind])
        mediumImage = str(df["mediumImage"][ind])
        highImage = str(df["highImage"][ind])
        roadgoat_url = str(df['roadgoat_url'][ind])
        destination_type = str(df['destination_type'][ind])
        geonames_id = str(df['geonames_id'][ind])
        walk_score_url = str(df['walk_score_url'][ind])
        budget = int(df['budget'][ind])
        safety = int(df['safety'][ind])
        covid = float(df['covid'][ind])
        average_rating = float(df['average_rating'][ind])
        foursquare_url = str(df['foursquare_url'][ind])
        kayak_car_rental_url = str(df['kayak_car_rental_url'][ind])
        kayak_lodging_url = str(df['kayak_lodging_url'][ind])
        airbnb_url = str(df['airbnb_url'][ind])
        getyourguide_url = str(df['getyourguide_url'][ind])
        known_for = str(df['known_for'][ind])
        photos = str(df['photos'][ind])
        mentions = str(df['mentions'][ind])

        city = City(
            cityId=cityId,
            cityName=cityName,
            state=state,
            stateCode=stateCode,
            country=country,
            countryCode=countryCode,
            elevationMeters=elevationMeters,
            latitude=latitude,
            longitude=longitude,
            population=population,
            timezone=timezone,
            defaultImage=defaultImage,
            mediumImage=mediumImage,
            highImage=highImage,
            roadgoat_url = roadgoat_url,
            destination_type = destination_type,
            geonames_id = geonames_id,
            walk_score_url = walk_score_url,
            budget = budget,
            safety = safety,
            covid = covid,
            average_rating = average_rating,
            foursquare_url = foursquare_url,
            kayak_car_rental_url = kayak_car_rental_url,
            kayak_lodging_url = kayak_lodging_url,
            airbnb_url = airbnb_url,
            getyourguide_url = getyourguide_url,
            known_for = known_for,
            photos = photos,
            mentions = mentions
        )
        city_list.append(city)

def add_companies():
    df = pd.read_csv(r"data/companies_updated.csv")
    print(df)
    for ind in df.index:
        companyId = str(df["companyId"][ind])
        companyName = str(df["name"][ind])
        description = str(df["description"][ind])
        employees = str(df["employees"][ind])
        founded = str(df["founded"][ind])
        hqLocation = str(df["hq_location"][ind])
        url = str(df["url"][ind])
        urlText = str(df["urlText"][ind])
        indeedFinalUrl = str(df["indeed_final_url"][ind])
        logoUrl = str(df["logo_url"][ind])
        rating = float(df["rating"][ind])
        revenue = str(df["revenue"][ind])
        reviewCount = int(df["review_count"][ind])
        sector = str(df["sector"][ind])
        facebookId = str(df["facebookId"][ind])
        twitterId = str(df["twitterId"][ind])
        employee_lower_bound = int(df['employee_lower_bound'][ind])
        employee_upper_bound = int(df['employee_upper_bound'][ind])


        company = Company(
            companyId=companyId,
            companyName=companyName,
            description=description,
            employees=employees,
            founded=founded,
            hqLocation=hqLocation,
            url=url,
            urlText=urlText,
            indeedFinalUrl=indeedFinalUrl,
            logoUrl=logoUrl,
            rating=rating,
            revenue=revenue,
            reviewCount=reviewCount,
            sector=sector,
            facebookId=facebookId,
            twitterId=twitterId,
            employee_lower_bound = employee_lower_bound,
            employee_upper_bound = employee_upper_bound
        )
        company_list.append(company)


if __name__ == "__main__":
    #    db.session.remove()
    # make sure we don't change the known_for table
    City.__table__.drop()
    Company.__table__.drop()
    Job.__table__.drop()
    
    City.__table__.create()
    Company.__table__.create()
    Job.__table__.create()

    # populate local lists
    add_jobs()
    add_cities()
    add_companies()

    # add all back to database
    db.session.add_all(job_list)
    db.session.add_all(city_list)
    db.session.add_all(company_list)
    db.session.commit()
