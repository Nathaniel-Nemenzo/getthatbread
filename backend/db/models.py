import os
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
CORS(app)

"""
RDS CONFIG
"""
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://{RDS_USERNAME}:{RDS_PASSWORD}@{RDS_HOSTNAME}:{RDS_PORT}/{RDS_DB_NAME}".format(
    RDS_USERNAME="getthatbread",
    RDS_PASSWORD="getthatbread-password",
    RDS_HOSTNAME="getthatbread-database.ce2yiy2d6ame.us-west-2.rds.amazonaws.com",
    RDS_PORT=5432,
    RDS_DB_NAME="postgres",
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Job(db.Model):
    __tablename__ = "job"
    jobId = db.Column(db.String(), primary_key=True)
    jobTitle = db.Column(db.String())
    description = db.Column(db.String())
    jobType = db.Column(db.String())
    creationDate = db.Column(db.String())
    salary = db.Column(db.String())
    indeedFinalUrl = db.Column(db.String())
    companyName = db.Column(db.String())
    companyId = db.Column(db.String())
    cityId = db.Column(db.String())
    country = db.Column(db.String())
    locality = db.Column(db.String())
    location = db.Column(db.String())
    locationZip = db.Column(db.String())
    salary_lower_bound = db.Column(db.Float)
    salary_upper_bound = db.Column(db.Float)
    salary_when = db.Column(db.String())

class Company(db.Model):
    __tablename__ = "company"
    companyId = db.Column(db.String(), primary_key=True)
    companyName = db.Column(db.String())
    description = db.Column(db.String())
    employees = db.Column(db.String())
    founded = db.Column(db.String())
    hqLocation = db.Column(db.String())
    url = db.Column(db.String())
    urlText = db.Column(db.String())
    indeedFinalUrl = db.Column(db.String())
    logoUrl = db.Column(db.String())
    rating = db.Column(db.Float)
    revenue = db.Column(db.String())
    reviewCount = db.Column(db.Integer)
    sector = db.Column(db.String())
    facebookId = db.Column(db.String())
    twitterId = db.Column(db.String())
    employee_lower_bound = db.Column(db.Integer)
    employee_upper_bound = db.Column(db.Integer)


class City(db.Model):
    __tablename__ = "city"
    cityId = db.Column(db.String(), primary_key=True)
    cityName = db.Column(db.String())
    state = db.Column(db.String())
    stateCode = db.Column(db.String())
    country = db.Column(db.String())
    countryCode = db.Column(db.String())
    elevationMeters = db.Column(db.Integer)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    population = db.Column(db.Integer)
    timezone = db.Column(db.String())
    defaultImage = db.Column(db.String())
    mediumImage = db.Column(db.String())
    highImage = db.Column(db.String())
    roadgoat_url = db.Column(db.String())
    destination_type = db.Column(db.String())
    geonames_id = db.Column(db.String())
    walk_score_url = db.Column(db.String())
    budget = db.Column(db.Integer)
    safety = db.Column(db.Integer)
    covid = db.Column(db.Float)
    average_rating = db.Column(db.Float)
    foursquare_url = db.Column(db.String())
    kayak_car_rental_url = db.Column(db.String())
    kayak_lodging_url = db.Column(db.String())
    airbnb_url = db.Column(db.String())
    getyourguide_url = db.Column(db.String())
    known_for = db.Column(db.String())
    photos = db.Column(db.String())
    mentions = db.Column(db.String())

class KnownFor(db.Model):
    __tablename__ = 'known_for'
    id = db.Column(db.Integer, primary_key = True)
    url = db.Column(db.String())