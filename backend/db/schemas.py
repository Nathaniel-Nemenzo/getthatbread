from db.models import app
from flask_marshmallow import Marshmallow, fields, Schema

ma = Marshmallow(app)

class JobSchema(Schema):
    class Meta:
        fields = (
            "jobId",
            "jobTitle",
            "description",
            "jobType",
            "creationDate",
            "salary",
            "indeedFinalUrl",
            "companyName",
            "companyId",
            "cityId",
            "country",
            "locality",
            "location",
            "locationZip",
            'salary_lower_bound',
            'salary_upper_bound',
            'salary_when'
        )


class CompanySchema(ma.Schema):
    class Meta:
        fields = (
            "companyId",
            "companyName",
            "description",
            "employees",
            "founded",
            "hqLocation",
            "url",
            "urlText",
            "indeedFinalUrl",
            "logoUrl",
            "rating",
            "revenue",
            "reviewCount",
            "sector",
            "facebookId",
            "twitterId",
            'employee_lower_bound',
            'employee_upper_bound'
        )


class CitySchema(ma.Schema):
    class Meta:
        fields = (
            "cityName",
            "state",
            "stateCode",
            "country",
            "countryCode",
            "elevationMeters",
            "latitude",
            "longitude",
            "population",
            "timezone",
            "defaultImage",
            "mediumImage",
            "highImage",
            "cityId",
            "roadgoat_url"
            "destination_type",
            "geonames_id",
            "walk_score_url",
            "budget",
            "safety",
            "covid",
            "average_rating",
            "foursquare_url",
            "kayak_car_rental_url",
            "kayak_lodging_uyrl",
            "airbnb_url",
            "getyourguide_url",
            "known_for",
            "photos",
            "mentions"
        )
    
class KnownForSchema(Schema):
    class Meta:
        fields = (
            "id",
            "url"
        )


class JobsSchema(ma.Schema):
    class Meta:
        fields = (
            "jobId",
            "jobTitle",
            "jobType",
            "creationDate",
            "salary",
            "companyName",
            "companyId",
            "cityId",
            "location",
            'salary_lower_bound',
            'salary_upper_bound',
            'salary_when'
        )


class CompaniesSchema(ma.Schema):
    class Meta:
        fields = (
            "companyId",
            "companyName",
            "employees",
            "hqLocation",
            "rating",
            "sector",
            'employee_lower_bound',
            'employee_upper_bound'
        )


class CitiesSchema(ma.Schema):
    class Meta:
        fields = (
            "cityId", 
            "cityName", 
            "state", 
            "country", 
            "population", 
            "timezone", 
            'average_rating',
            'budget',
            'safety',
            'covid')


job_schema = JobSchema()
city_schema = CitySchema()
company_schema = CompanySchema()
known_for_schema = KnownForSchema()

cities_schema = CitySchema(many=True)
jobs_schema = JobSchema(many=True)
companies_schema = CompanySchema(many=True)