'''
add_roadgoat_data.py: script to add cached roadgoat data from jsonfile into our csv files
'''

from locale import D_FMT
import pandas as pd
import csv
from json import loads

_CSV_FILE = '../data/cities_new.csv'
_ROADGOAT_FILE = 'roadgoat_data.json'

def add_columns():
    city_data = pd.read_csv(_CSV_FILE)
    empty_column = [''] * 102
    print(empty_column)
    
    # add the columns with all empty data
    # attribute columns
    city_data['roadgoat_url'] = empty_column
    city_data['destination_type'] = empty_column
    city_data['geonames_id'] = empty_column
    city_data['walk_score_url'] = empty_column
    city_data['budget'] = empty_column
    city_data['safety'] = empty_column
    city_data['covid'] = empty_column
    city_data['average_rating'] = empty_column
    city_data['foursquare_url'] = empty_column
    city_data['kayak_car_rental_url'] = empty_column
    city_data['kayak_lodging_url'] = empty_column
    city_data['airbnb_url'] = empty_column
    city_data['getyourguide_url'] = empty_column

    # relation columns
    city_data['known_for'] = empty_column
    city_data['photos'] = empty_column
    city_data['mentions'] = empty_column

    city_data.to_csv('../data/cities_new.csv')

if __name__ == '__main__':
    '''
    general workflow:
    create the columns in the csv each with empty strings
    iterate for each jstring in the roadgoat json data file:
        for each jstring, parse out the remaining data and write that row to the csv
    '''

    # add_columns()
    # get new city data csv in dataframe (0-indexed)
    df = pd.read_csv(_CSV_FILE)

    with open(_ROADGOAT_FILE) as infile:
        for idx, line in enumerate(infile):
            # load json from file
            json = loads(line)

            # dictionary of [id, type, attributes, relationships]
            data = json['data']

            # list of 'included' objects with IDs matching
            included = json['included']

            # get outer fields
            id = data['id']
            type = data['type']
            attributes = data['attributes']
            relationships = data['relationships']

            # get inner fields of 'attributes'
            roadgoat_url = attributes['url']
            destination_type = attributes['destination_type']
            geonames_id = attributes['geonames_id']
            walk_score_url = attributes['walk_score_url']
            budget = attributes['budget'][list(attributes['budget'].keys())[0]]['value']# smallest scale, only id
            safety = attributes['safety'][list(attributes['safety'].keys())[0]]['value'] # smallest scale, only id
            covid = attributes['covid'][list(attributes['covid'].keys())[0]]['value'] # people / 100k
            average_rating = attributes['average_rating']
            foursquare_url = attributes['foursquare_url']
            kayak_car_rental_url = attributes['kayak_car_rental_url']
            kayak_lodging_url = attributes['kayak_lodgings_url']
            airbnb_url = attributes['airbnb_url']
            getyourguide_url = attributes['getyourguide_url']

            # get inner fields of relationships
            known_for = relationships['known_for'] if 'known_for' in relationships else {'data': []}
            photos = relationships['photos'] if 'photos' in relationships else {'data': []}
            mentions = relationships['mentions'] if 'mentions' in relationships else {'data': []}

            # parse known_for
            known_for_ids = []
            for kf in known_for['data']:
                known_for_ids.append(kf['id'])

            # parse photos
            photos_data = {}
            for photo in photos['data']:
                photos_data[photo['id']] = ''

            # parse mentions
            mentions_data = {}
            for mention in mentions['data']:
                mentions_data[mention['id']] = ''

            # go through includes and add to dicts
            for include in included:
                # check different cases
                if include['type'] == 'photo':
                    photos_data[include['id']] = include['attributes']['image']['full']
                elif include['type'] == 'mention':
                    mentions_data[include['id']] = include['attributes']['url']

            # put data in df
            df.at[idx, 'roadgoat_url'] = roadgoat_url
            df.at[idx, 'destination_type'] = destination_type
            df.at[idx, 'geonames_id'] = geonames_id
            df.at[idx, 'walk_score_url'] = walk_score_url
            df.at[idx, 'budget'] = budget
            df.at[idx, 'safety'] = safety
            df.at[idx, 'covid'] = covid
            df.at[idx, 'average_rating'] = average_rating
            df.at[idx, 'foursquare_url'] = foursquare_url
            df.at[idx, 'kayak_car_rental_url'] = kayak_car_rental_url
            df.at[idx, 'kayak_lodging_url'] = kayak_lodging_url
            df.at[idx, 'airbnb_url'] = airbnb_url
            df.at[idx, 'getyourguide_url'] = getyourguide_url
            
            # parse known for array into strng
            known_for_str = ''
            for id in known_for_ids:
                known_for_str += f'{id};'
            df.at[idx, 'known_for'] = known_for_str[:-1]

            # parse photos dict into string
            photos_str = ''
            urls = photos_data.values()
            for url in urls:
                photos_str += f'{url};'
            df.at[idx, 'photos'] = photos_str[:-1]

            # parse mentions dict into string
            mentions_str = ''
            urls = mentions_data.values()
            for url in urls:
                mentions_str += f'{url};'
            df.at[idx, 'mentions'] = mentions_str[:-1]

    df.to_csv('../data/cities_updated.csv')