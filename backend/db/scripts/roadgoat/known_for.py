import json

kf = {}

for line in open('roadgoat_data.json'):
    d = json.loads(line)
    included = d['included']
    for include in included:
        if include['type'] == 'known_for':
            if include['id'] not in kf:
                kf[include['id']] = include['attributes']['slug']

print(kf)

'''
{'12': 'charming', '4': 'foodie', '3': 'nightlife', '17': 'architecture', '2': 'history', '8': 'museums', '7': 'performing-arts', '21': 'music', '9': 'posh', '18': 'lgbt-friendly', '20': 'diversity', '6': 'shopping', '10': 'hipster', '1': 'beach-town', '5': 'outdoorsy', '14': 'family-friendly', '19': 'wineries', '11': 'hippie', '13': 'college-town', '15': 'ski-town'}
'''