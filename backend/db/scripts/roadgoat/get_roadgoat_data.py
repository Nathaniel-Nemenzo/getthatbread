'''
add_cities_data.py: add additional data for cities from the RoadGoat api: https://developer.roadgoat.com/#introduction

additional columns:

attributes (some may be null if no data found)
----------
destination_type
geonames_id
walk_score_url 
budget (1-8), TODO: create table for budgets
safety (1,8), TODO: create table for safety
covid (value), TODO: create covid table
average_rating
foursquare_url
kayak_car_rental_url
kayak_lodging_url
airbnb_url
getyourguide_url

relationships
----------
continent
regions (if any)
known_for
photos
mentions
'''

import requests
import json

from requests.auth import HTTPBasicAuth

'''
ROADGOAT INFO
'''
_ROADGOAT_API_KEY = '937a7618fbf3472ae7811613ed6c3aa5'
_ROADGOAT_API_SECRET = '8861b2bb670433d088dab02ea7da0dfa'
_ROADGOAT_DATA_FILE = 'roadgoat_data.json'
_ROADGOAT_AUTOCOMPLETE_ENDPOINT = 'https://api.roadgoat.com/api/v2/destinations/auto_complete?'
_ROADGOAT_DESTINATIONS_ENDPOINT = 'https://api.roadgoat.com/api/v2/destinations/'

'''
GETTHATBREAD INFO
'''
_GETTHATBREAD_CITY_ENDPOINT = 'https://api.getthatbread.me/api/cities'

def get_city_names():
    '''
    get all cities from our endpoint
    '''
    res = []
    response = requests.get(_GETTHATBREAD_CITY_ENDPOINT).json()
    data = response['data']
    for city in data:
        res.append(city['cityId'])
    return res

def get_roadgoat_data(city):
    '''
    gets roadgoat data for a city
    1. first uses the autocompletion endpoint to get unique roadgoat id
    2. then uses roadgoat id to get fill json response for specific city
    '''
    ac_response = requests.get(_ROADGOAT_AUTOCOMPLETE_ENDPOINT + f'q={city}', auth = (_ROADGOAT_API_KEY, _ROADGOAT_API_SECRET)).json()

    # parse out fields and choose the first city in the result
    data = ac_response['data']
    ac_city = data[0]
    id = ac_city['id']

    # make second api request to get actual city
    response = requests.get(_ROADGOAT_DESTINATIONS_ENDPOINT + id, auth = (_ROADGOAT_API_KEY, _ROADGOAT_API_SECRET))
    return response.json()

def write_roadgoat_data(city_data):
    '''
    writes the roadgoat response for one city
    '''
    with open(_ROADGOAT_DATA_FILE, 'a') as outfile:
        json.dump(city_data, outfile)
        outfile.write('\n')

if __name__ == '__main__':
    cities = get_city_names()
    print(f'retrieved city names | len = {len(cities)}')
    print('writing data now')
    for city in cities:
        data = get_roadgoat_data(city)
        write_roadgoat_data(data)
    print('done')
