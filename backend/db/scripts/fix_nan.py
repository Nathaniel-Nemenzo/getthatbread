import pandas as pd

_CSV_FILE = '../data/cities_updated.csv'

df = pd.read_csv(_CSV_FILE)
df.fillna(0)
df.to_csv('../data/cities_updated1.csv')