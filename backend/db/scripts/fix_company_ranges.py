'''
script to fix company revenues
'''
import pandas as pd
import numpy as np
import re

_CSV_FILE = '../data/companies.csv'

def add_columns(df):
    empty_column = [''] * 288
    df['employee_lower_bound'] = empty_column
    df['employee_upper_bound'] = empty_column
    return df

def change_lower(row):
    # check nan
    if type(row['employees']) == float and np.isnan(row['employees']):
        return 0
    
    # check if 10000+
    if '+' in row['employees']:
        return 10000
    employees = row['employees'].split('to')[0]
    return int(re.sub("[^\d\.]", "", employees))

def change_upper(row):
    # check nan
    if type(row['employees']) == float and np.isnan(row['employees']):
        return 0

    # check if 10000+
    if '+' in row['employees']:
        return 10000

    employees = row['employees'].split('to')[1]
    return int(re.sub("[^\d\.]", "", employees))

if __name__ == "__main__":
    df = pd.read_csv(_CSV_FILE)

    # add columns
    add_columns(df)

    # change rows
    for i, row in df.iterrows():
        df.loc[i, 'employee_lower_bound'] = change_lower(row)
        df.loc[i, 'employee_upper_bound'] = change_upper(row)

    # write back
    df.to_csv('../data/companies_updated.csv')