import os
import json
import requests
import pandas as pd
import time

with open("data/get_job.json", "r") as file:
    search_data = json.load(file)
city_list = search_data["city"]
title_list = search_data["title"]

with open("data/get_city.json", "r") as file:
    search_data = json.load(file)
city_more = search_data["city"]

cities = []
jobs = []
companies = []
API_KEY = "52239ac23bmshe253fdad681f260p159417jsn3cf65e8a7bf3"
JOB_HOST = "indeed12.p.rapidapi.com"
CITY_HOST = "wft-geo-db.p.rapidapi.com"
IMAGE_HOST = "youtube-v31.p.rapidapi.com"
JOB_PER_TYPE = 5


def search_job(city, title, page):
    headers = {"x-rapidapi-host": JOB_HOST, "x-rapidapi-key": API_KEY}
    url = "https://{}/jobs/search".format(JOB_HOST)
    querystring = {"query": title, "location": city, "page_id": page}
    response = requests.request("GET", url, headers=headers, params=querystring)
    return response


def job_detail(job_id):
    headers = {"x-rapidapi-host": JOB_HOST, "x-rapidapi-key": API_KEY}
    url = "https://{0}/job/{1}".format(JOB_HOST, job_id)
    response = requests.request("GET", url, headers=headers)
    return response


def company_detail(company_id):
    headers = {"x-rapidapi-host": JOB_HOST, "x-rapidapi-key": API_KEY}
    url = "https://{0}/company/{1}".format(JOB_HOST, company_id)
    response = requests.request("GET", url, headers=headers)
    return response


def city_detail(city):
    headers = {"x-rapidapi-host": CITY_HOST, "x-rapidapi-key": API_KEY}
    url = "https://{}/v1/geo/cities".format(CITY_HOST)
    temp = city.split(",")
    querystring = {
        "limit": "5",
        "countryIds": "US",
        "minPopulation": "150000",
        "namePrefix": temp[0],
        "types": "CITY",
    }
    response = requests.request("GET", url, headers=headers, params=querystring)
    response = response.json().get("data")
    result = None
    for item in response:
        if item["city"] + ", " + item["regionCode"] == city:
            city_id = item["wikiDataId"]
            url = "https://{0}/v1/geo/cities/{1}".format(CITY_HOST, city_id)
            result = requests.request("GET", url, headers=headers)
            break
    return result


def city_image(city):
    url = "https://{}/search".format(IMAGE_HOST)
    querystr = "{} tour".format(city)
    querystring = {
        "q": querystr,
        "part": "snippet,id",
        "regionCode": "US",
        "maxResults": "1",
    }
    headers = {"x-rapidapi-host": IMAGE_HOST, "x-rapidapi-key": API_KEY}
    response = requests.request("GET", url, headers=headers, params=querystring)
    response = response.json()
    if response.get("items") and len(response["items"]) > 0:
        default_image = response["items"][0]["snippet"]["thumbnails"]["default"]["url"]
        medium_image = response["items"][0]["snippet"]["thumbnails"]["medium"]["url"]
        high_image = response["items"][0]["snippet"]["thumbnails"]["high"]["url"]
        return {
            "defaultImage": default_image,
            "mediumImage": medium_image,
            "highImage": high_image,
        }
    else:
        return {"defaultImage": "", "mediumImage": "", "highImage": ""}


def get_data():
    company_table = {}
    for city in city_list:
        for title in title_list:
            count = 0
            page = 0
            while count < JOB_PER_TYPE:
                page = page + 1
                response = search_job(city, title, str(page))
                job_info = response.json().get("hits")
                for item in job_info:
                    job_id = item.get("id")
                    response = job_detail(job_id)
                    response = response.json()
                    if not response.get("company").get("id"):
                        continue
                    item.update(response)
                    item["cityId"] = city
                    item["companyId"] = item.get("company").get("id")
                    item.pop("link", None)
                    item.pop("company", None)
                    jobs.append(item)
                    company_id = item.get("companyId")
                    if company_id and not company_id in company_table:
                        response = company_detail(company_id)
                        response = response.json()
                        response["companyId"] = company_id
                        company_url = response["company_links"]
                        if company_url:
                            response["url"] = company_url[0]["url"]
                            response["urlText"] = company_url[0]["text"]
                        sector = response["sectors"]
                        if sector:
                            response["sector"] = sector[0]
                        response.pop("company_links", None)
                        response.pop("sectors", None)
                        companies.append(response)
                        company_table[company_id] = "y"
                    count = count + 1
                    if count == JOB_PER_TYPE:
                        break

    for city in city_more:
        response = city_detail(city)
        if not response is None:
            response = response.json()
            response = response.get("data")
            image = city_image(city)
            response.update(image)
            response["cityId"] = city
            response.pop("id", None)
            response.pop("type", None)
            response.pop("distance", None)
            response.pop("deleted", None)
            response.pop("placeType", None)
            cities.append(response)

    jobsDataFrames = pd.DataFrame.from_dict(jobs)
    jobsDataFrames.to_csv("data/jobs.csv", encoding="utf-8")
    citiesDataFrames = pd.DataFrame.from_dict(cities)
    citiesDataFrames.to_csv("data/cities.csv", encoding="utf-8")
    companiesDataFrames = pd.DataFrame.from_dict(companies)
    companiesDataFrames.to_csv("data/companies.csv", encoding="utf-8")


if __name__ == "__main__":
    print("Populating CSV...")
    get_data()
    print("Done")
