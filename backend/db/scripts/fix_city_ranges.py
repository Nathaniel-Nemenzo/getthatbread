import pandas as pd
import numpy as np
import re

_CSV_FILE = '../data/jobs.csv'

def add_columns(df):
    empty_column = [''] * 410
    df['salary_lower_bound'] = empty_column
    df['salary_upper_bound'] = empty_column
    df['salary_when'] = empty_column
    return df

def change_when(row):
    str = row['salary']
    if type(str) == float and np.isnan(str):
        return 'n/a'
    elif '-' in str:
        return 'hour' if 'hour' in str else 'yearly'
    elif 'From' in str:
        return 'hour'
    elif 'a year' in str:
        return 'year'
    elif 'month' in str.lower():
        return 'month'

def change_lower(row):
    str = row['salary']
    if type(str) == float and np.isnan(str):
        return 0
    elif '-' in str:
        splt = str.split('-')
        return re.sub("[^\d\.]", "", splt[0])
    elif 'From' in str or 'a year' in str or 'hour' in str or 'month' in str:
        return re.sub("[^\d\.]", "", str)
    return 0

def change_upper(row):
    str = row['salary']
    if type(str) == float and np.isnan(str):
        return 0
    elif '-' in str:
        splt = str.split('-')
        return re.sub("[^\d\.]", "", splt[1])
    elif 'From' in str or 'a year' in str or 'hour' in str or 'month' in str:
        return re.sub("[^\d\.]", "", str)
    return 0

if __name__ == "__main__":
    # read in dataframe
    df = pd.read_csv(_CSV_FILE)

    # add the columns
    add_columns(df)

    # change the rows
    for i, row in df.iterrows():
        df.loc[i, 'salary_upper_bound'] = change_upper(row)
        df.loc[i, 'salary_when'] = change_when(row)
        df.loc[i, 'salary_lower_bound'] = change_lower(row)

    # write back to csv file
    df.to_csv('../data/jobs_updated.csv')