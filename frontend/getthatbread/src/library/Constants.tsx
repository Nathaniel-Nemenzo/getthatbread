const MAX_INSTANCES_PER_PAGE = 18;

const COMPANY_BENEFITS = [
  "healthcare",
  "dental care",
  "transportation",
  "corporate housing",
  "paid time-off",
  "401(k)",
];

const COMPANY_SECTORS = [
  "Aerospace",
  "Business",
  "Construction",
  "Defense",
  "Energy",
  "Entertainment",
  "Food & Beverage",
  "Government",
  "Information",
  "Insurance",
  "Healthcare",
  "Human Resources",
  "Manufacturing",
  "Mining",
  "Nonprofit",
  "Pharmaceutical",
  "Real Estate",
  "Restaurants",
  "Retail & Wholesale",
  "Science",
  "Software",
  "Technology",
  "Transportation",
];

const JOB_SORT_OPTIONS = [
  "Name (A-Z)",
  "Name (Z-A)",
  "Company (A-Z)",
  "Company (Z-A)",
  "Salary (low to high)",
  "Salary (high to low)",
];

const COMPANY_SORT_OPTIONS = [
  "Name (A-Z)",
  "Name (Z-A)",
  "Location (A-Z)",
  "Location (Z-A)",
  "Sector (A-Z)",
  "Sector (Z-A)",
  "Number of employees (low to high)",
  "Number of employees (high to low)",
  "Rating (low to high)",
  "Rating (high to low)",
];

const CITY_SORT_OPTIONS = [
  "Name (A-Z)",
  "Name (Z-A)",
  "Budget Rating (low to high)",
  "Budget Rating (high to low)",
  "Safety Rating (low to high)",
  "Safety Rating (high to low)",
  "COVID-19 Levels (low to high)",
  "COVID-19 Levels (high to low)",
  "Rating (low to high)",
  "Rating (high to low)",
];

export {
  MAX_INSTANCES_PER_PAGE,
  COMPANY_BENEFITS,
  COMPANY_SECTORS,
  JOB_SORT_OPTIONS,
  COMPANY_SORT_OPTIONS,
  CITY_SORT_OPTIONS,
};
