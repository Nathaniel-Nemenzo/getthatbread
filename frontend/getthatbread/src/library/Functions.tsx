import { BsStarFill, BsStarHalf, BsStar } from "react-icons/bs";

/**
 * Formats the company name to get rid of any .coms
 * and dashes to pass into the clearbit api for
 * company logos.
 * @param company
 * @returns formatted company name
 */
const getLogoLink = (company: string) => {
  return company.replace(/\s/g, "").replace(/\..*$/, "");
};

/**
 * Fixes the string to display a default value if there
 * was no value for this field in the API.
 * @param value
 * @param def
 * @returns value or default if value is NAN
 */
const checkUnknowns = (value: string, def: string) => {
  if (!value.localeCompare("nan", undefined, { sensitivity: "accent" })) {
    return def;
  }
  return value;
};

/**
 * Format the number to have commas between thousands.
 * 1000 => 1,000
 * @param value
 * @returns value formatted with correct commas
 */
const formatNumbers = (value: number) => {
  if (value === -1) {
    return "Unknown";
  }
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

/**
 * Returns a given number of star icons depending on the rating.
 * Handles full and half star ratings.
 * @param rating
 * @param limit
 * @returns star icons to represent the rating
 */
const getStarRating = (rating: number, limit: number) => {
  let roundedStars = Math.round(rating * 2) / 2;
  var resultStars = [];
  // display half stars
  for (let count = 0; count < limit; count++) {
    if (roundedStars >= 1) {
      resultStars.push(<BsStarFill style={{ verticalAlign: "text-top" }} />);
      roundedStars--;
    } else if (roundedStars === 0.5) {
      resultStars.push(<BsStarHalf style={{ verticalAlign: "text-top" }} />);
      roundedStars -= 0.5;
    } else {
      resultStars.push(<BsStar style={{ verticalAlign: "text-top" }} />);
      roundedStars--;
    }
  }
  return resultStars;
};

export { getLogoLink, checkUnknowns, formatNumbers, getStarRating };
