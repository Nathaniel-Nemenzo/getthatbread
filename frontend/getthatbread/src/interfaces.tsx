interface ModelInterface {
  type: string;
  id: string;
  icon: string;
  title: string;
  subtitle: string;
  keywords?: string[];
  additional_info?: string[];
  description?: string[];

  // for formatting issues
  card_size?: number;
}

interface JobInterface extends ModelInterface {}

interface CityInterface extends ModelInterface {
  city: string;
  state: string;
  country: string;
  country_code: string;
}

interface CompanyInterface extends ModelInterface {}

export { ModelInterface, JobInterface, CityInterface, CompanyInterface };
