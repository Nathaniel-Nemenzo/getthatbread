import React from "react";

import "./footer.css";

class Footer extends React.Component {
  render() {
    return (
      <>
        <footer
          bg="primary"
          expand="lg"
          variant="dark"
          className="page-footer font-small blue"
        >
          <div className="text-center col">
            © 2021 Copyright&nbsp;
            <a href="https://getthatbread.me/">GetThatBread</a>
          </div>
        </footer>
      </>
    );
  }
}

export default Footer;
