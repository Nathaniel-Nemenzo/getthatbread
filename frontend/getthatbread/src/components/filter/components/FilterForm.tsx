import React, { useState } from "react";
import {
  Row,
  Col,
  Button,
  Modal,
  DropdownButton,
  Dropdown,
  ButtonGroup,
} from "react-bootstrap";

import "../Filter.css";

function FilterForm(props: {
  children: JSX.Element;
  onSubmit?: () => void;
  sortOptions?: string[];
  onSort?: (option: string) => void;
}) {
  const [show, setShow] = useState(false);

  const getSortingOptions = () => {
    return props.sortOptions?.map((item, index) => {
      return (
        <Dropdown.Item
          eventKey={`${index}`}
          id={item}
          key={item}
          onClick={() => {
            if (props.onSort !== undefined) {
              props.onSort(item);
            }
          }}
        >
          {item}
        </Dropdown.Item>
      );
    });
  };

  const handleClose = () => {
    if (props.onSubmit !== undefined) {
      props.onSubmit();
    }
    setShow(false);
  };
  const handleCancel = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <Row className="filter-form">
      <Col>
        <Button
          variant="outline-primary"
          onClick={handleShow}
          style={{ width: "100%" }}
        >
          All Filters
        </Button>
        <Modal show={show} onHide={handleCancel}>
          <Modal.Header closeButton>
            <Modal.Title>Filters</Modal.Title>
          </Modal.Header>
          <Modal.Body>{props.children}</Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={handleCancel}
              className={"filter-cancel"}
            >
              Cancel
            </Button>

            <Button
              variant="primary"
              onClick={handleClose}
              className={"filter-apply"}
            >
              Apply filters
            </Button>
          </Modal.Footer>
        </Modal>
      </Col>
      <Col>
        <DropdownButton
          as={ButtonGroup}
          title="Sort by"
          style={{ width: "100%" }}
        >
          {getSortingOptions()}
        </DropdownButton>
      </Col>
    </Row>
  );
}

export default FilterForm;
