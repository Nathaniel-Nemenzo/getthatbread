import React from "react";
import { Container, Row, Col, Form } from "react-bootstrap";

import FilterForm from "./components/FilterForm";
import SearchBar from "../search/searchbar/SearchBar";

import "./Filter.css";

function Filter(props: {
  children: JSX.Element;
  searchPlaceholder: string;
  onSearchChange: (searchPhrase: string) => void;
  onSearchEnter?: () => void;
  onSubmitForm?: () => void;
  sortOptions?: string[];
  onSortChange?: (sortPhrase: string) => void;
}) {
  return (
    <Container className="mt-4">
      <Row className="filter-margin">
        <Col lg={4}>
          <FilterForm
            onSubmit={props.onSubmitForm}
            sortOptions={props.sortOptions}
            onSort={props.onSortChange}
          >
            {props.children}
          </FilterForm>
        </Col>
        <Col className="filter-body">
          <Form className="filter-main">
            <SearchBar
              placeholder={props.searchPlaceholder}
              onChange={props.onSearchChange}
              onEnter={props.onSearchEnter}
            />
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Filter;
