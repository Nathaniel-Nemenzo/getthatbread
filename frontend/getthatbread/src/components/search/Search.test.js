import React from "react";
import renderer from "react-test-renderer";

import SearchHeader from "./SearchHeader";
import SearchTable from "./searchtable/SearchTable";
import SearchCard from "./searchgrid/SearchCard";
import SearchGrid from "./searchgrid/SearchGrid";
import SearchBar from "./searchbar/SearchBar";

test("search header properly renders", () => {
  const headerProps = {
    start: 1,
    end: 10,
    total: 100,
  };

  const component = renderer.create(<SearchHeader props={headerProps} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("search table properly renders with dummy data", () => {
  let data = [
    {
      type: "test type",
      id: 1,
      icon: "test icon",
      title: "test title",
      subtitle: "test subtitle",
    },
  ];
  const component = renderer.create(<SearchTable data={data} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("search card properly renders", () => {
  let data = {
    type: "test type",
    id: 1,
    icon: "test icon",
    title: "test title",
    subtitle: "test subtitle",
    card_size: 1,
  };
  const component = renderer.create(<SearchCard data={data} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("search grid properly renders with dummy data", () => {
  let data = [
    {
      type: "test type",
      id: 1,
      icon: "test icon",
      title: "test title",
      subtitle: "test subtitle",
      card_size: 1,
    },
  ];
  const component = renderer.create(<SearchGrid data={data} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("search bar properly renders", () => {
  const component = renderer.create(<SearchBar />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
