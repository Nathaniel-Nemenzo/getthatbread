import React from "react";
import { Row, Col, Card } from "react-bootstrap";

import { ModelInterface } from "../../../interfaces";
import { SearchHeaderInterface } from "../interfaces";
import SearchHeader from "../SearchHeader";
import SearchTableRow from "./components/SearchTableRow";

import "../search.css";

function SearchTable(props: { data: ModelInterface[] }) {
  const { data } = props;
  const headerProps: SearchHeaderInterface = {
    start: 1,
    end: 6,
    total: 6,
  };
  /**
   * total | sort | etc.
   * ------
   * table
   */
  return (
    <Row>
      <Col lg={12}>
        <Card>
          <Card.Body>
            <Row className={"search-body"}>
              <Col lg={12}>
                <div className="search-result">
                  <div className="result-header">
                    <SearchHeader props={headerProps} />
                  </div>
                  <div className="result-body">
                    <div className="table-responsive">
                      <table className="table widget-search-table">
                        <tbody>{data.map(SearchTableRow)}</tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

export default SearchTable;
