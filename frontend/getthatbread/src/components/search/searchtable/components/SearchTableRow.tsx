import React, { useState } from "react";
import { ModelInterface } from "../../../../interfaces";
import { Navigate } from "react-router-dom";

import "../../search.css";

function SearchTableRow(data: ModelInterface) {
  const [redirect, setRedirect] = useState(false);

  if (redirect) {
    return <Navigate to={`/${data.type}/${data.id}`} />;
  }

  // var additional_info: JSX.Element[] = (data.additional_info !== undefined) ? data.additional_info.map(renderAdditionalInfo) : [];
  // var keywords: JSX.Element[] = (data.keywords !== undefined) ? data.keywords.map(renderAttributes) : [];
  return (
    <tr onClick={() => setRedirect(true)} key={data.id}>
      <td>
        <div className="widget-search-table-icon-img">
          <img src={data.icon} alt="icon"></img>
        </div>
      </td>
      <td>
        <div className="widget-search-table-title">
          {data.title}
          <p className="m-0 widget-search-table-subtitle">
            {data.subtitle}
            <span className="text-muted time" />
          </p>
        </div>
      </td>
      {/* {additional_info}
            <td>
                <div className="widget-search-table-keywords bg-soft-base">
                    {keywords}
                </div>
            </td> */}
    </tr>
  );
}

export default SearchTableRow;
