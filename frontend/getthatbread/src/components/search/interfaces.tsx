interface SearchHeaderInterface {
  start: number;
  end: number;
  total: number;
}

export { SearchHeaderInterface };
