import React from "react";
import { Row, Col, Card } from "react-bootstrap";

import { ModelInterface } from "../../../interfaces";
import { SearchHeaderInterface } from "../interfaces";
import SearchHeader from "../SearchHeader";
import SearchCard from "./SearchCard";

const MAX_INSTANCES = 18;

function SearchGrid(props: {
  data: ModelInterface[];
  searchQuery?: string;
  pageNum: number;
  totalInstances: number;
  pagination: React.ReactNode;
}) {
  const { data, pageNum, totalInstances, pagination } = props;

  // if page num 1 => 1 - 18
  // if page num 2 => 19 - 36
  const headerProps: SearchHeaderInterface = {
    start: (pageNum - 1) * MAX_INSTANCES + 1,
    end: (pageNum - 1) * MAX_INSTANCES + data.length,
    total: totalInstances,
  };

  return (
    <Row>
      <Col lg={12}>
        <Card className={"search-grid"}>
          <Card.Body>
            <Row className={"search-body"}>
              <Col lg={12}>
                <div className="search-result">
                  <div className="result-header">
                    <div style={{ marginBottom: ".25rem" }}>
                      <SearchHeader props={headerProps} />
                    </div>
                    {pagination}
                  </div>
                  <Row className="" xs={1} md={2}>
                    {data.map((item, index) => {
                      return (
                        <SearchCard
                          data={item}
                          searchQuery={props.searchQuery}
                          key={index}
                        />
                      );
                    })}
                  </Row>
                </div>
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

export default SearchGrid;
