import React, { useState } from "react";
import { Badge, Col, Card } from "react-bootstrap";
import { Navigate } from "react-router-dom";

import { ModelInterface } from "../../../interfaces";
import Highlighter from "react-highlight-words";

import "../search.css";

function SearchCard(props: { data: ModelInterface; searchQuery?: string }) {
  const { data } = props;
  const [redirect, setRedirect] = useState(false);

  const displayDescription = (info: string[]) => {
    // description label and data are paired together in the array
    let description = [];
    for (let i = 0; i < info.length; i++) {
      description.push(
        <span className="model-description" key={info[i]}>
          <Highlighter
            highlightClassName={"card-search-highlight"}
            searchWords={props.searchQuery ? [props.searchQuery] : []}
            textToHighlight={`${info[i]}`}
            key={info[i]}
          />
        </span>
      );
    }
    return description;
  };

  const displayBadges = (info: string[]) => {
    return info.map((item) => {
      if (item.localeCompare("unknown", undefined, { sensitivity: "accent" })) {
        return (
          <Badge className="badge-format" key={item}>
            <Highlighter
              highlightClassName={"card-search-highlight"}
              searchWords={props.searchQuery ? [props.searchQuery] : []}
              textToHighlight={item.toLowerCase()}
              key={item}
            />
          </Badge>
        );
      }
      return <span key={item}></span>;
    });
  };

  if (redirect) {
    return <Navigate to={`/${data.type}/${data.id}`} />;
  }

  return (
    <Col lg={data.card_size} key={data.id} className={`mb-4`}>
      <Card
        onClick={() => {
          setRedirect(true);
        }}
        key={data.id}
        className={"search-card clickable-card"}
      >
        <Card.Body>
          <Card.Title className="searchcard-title-icon">
            <img
              alt={"logo"}
              src={data.icon}
              onError={({ currentTarget }) => {
                currentTarget.onerror = null;
                currentTarget.src =
                  "https://www.pngitem.com/pimgs/m/208-2087944_question-mark-logo-whatsapp-png-transparent-png.png";
              }}
            />
            <span className="search-card-title card-links">
              <Highlighter
                highlightClassName={"card-search-highlight"}
                searchWords={props.searchQuery ? [props.searchQuery] : []}
                textToHighlight={data.title}
              />
            </span>
          </Card.Title>
          <Card.Subtitle className="mb-2">
            <Highlighter
              highlightClassName={"card-search-highlight"}
              searchWords={props.searchQuery ? [props.searchQuery] : []}
              textToHighlight={data.subtitle}
            />
          </Card.Subtitle>
          <Card.Text className="">
            {/* possibly change depending on apis */}
            {displayDescription(data.description ?? [])}
          </Card.Text>
        </Card.Body>
        <Card.Footer className={"search-grid-card-footer"}>
          <div>{displayBadges(data.keywords ?? [])}</div>
        </Card.Footer>
      </Card>
    </Col>
  );
}

export default SearchCard;
