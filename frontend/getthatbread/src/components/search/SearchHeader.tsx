import React from "react";

import { SearchHeaderInterface } from "./interfaces";

import "./search.css";

function SearchTableHeader(data: { props: SearchHeaderInterface }) {
  const { start, end, total } = data.props;
  return (
    <div className="row">
      <div className="col-lg-12 d-flex justify-content-center">
        <div className="records">
          Showing:{" "}
          <b>
            {start}-{end}
          </b>{" "}
          of <b>{total}</b> result
        </div>
      </div>
    </div>
  );
}

export default SearchTableHeader;
