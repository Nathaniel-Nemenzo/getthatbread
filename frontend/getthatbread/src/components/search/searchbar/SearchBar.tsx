import React from "react";
import { InputGroup, FormControl } from "react-bootstrap";

function SearchBar(props: {
  placeholder: string;
  onChange: (searchPhrase: string) => void;
  onEnter?: () => void;
}) {
  return (
    <InputGroup size="lg">
      <FormControl
        className="search-bar"
        type="search"
        placeholder={props.placeholder}
        id="search-bar-id"
        aria-label="Search"
        onChange={(e) => props.onChange(e.target.value)}
        onKeyDown={(event) => {
          if (event.key === "Enter" && props.onEnter !== undefined) {
            props.onEnter();
          }
        }}
      />
    </InputGroup>
  );
}

export default SearchBar;
