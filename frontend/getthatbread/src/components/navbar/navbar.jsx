import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";

import "./navbar.css";

class MyNavbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currSearch: "",
      finalSearch: "",
      submit: false,
    };
  }

  render() {
    return (
      <>
        <Navbar expand="lg" variant="dark" sticky="top" className={"navbar"}>
          <Container>
            <Navbar.Brand href="/">getthatbread.me</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className="d-flex">
              <Nav className="justify-content-start">
                <Nav.Link href="/job" id={"job-nav-link"}>
                  jobs
                </Nav.Link>
                <Nav.Link href="/company">companies</Nav.Link>
                <Nav.Link href="/city">cities</Nav.Link>
                <Nav.Link href="/search&amp;">search</Nav.Link>
                <Nav.Link href="/visuals">visualizations</Nav.Link>
                <Nav.Link href="/provider">provider visualizations</Nav.Link>
                <Nav.Link href="/about">about</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default MyNavbar;
