import React from "react";
import renderer from "react-test-renderer";
import Navbar from "./navbar.jsx";

test("navbar properly renders", () => {
  const component = renderer.create(<Navbar />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
