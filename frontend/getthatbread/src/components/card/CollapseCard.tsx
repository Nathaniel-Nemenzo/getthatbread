import React, { useState } from "react";
import { Card, Row, Col } from "react-bootstrap";

import { FaAngleRight, FaAngleDown } from "react-icons/fa";

import "./card.css";

function CollapseCard(props: { title: string; children: JSX.Element }) {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Card className={"collapse-card clickable-card"}>
      <Card.Header
        onClick={() => setIsOpen(!isOpen)}
        className={"collapse-card-header"}
      >
        <Row>
          <Col>
            <h3 style={{ marginBottom: "0px" }}>{props.title}</h3>
          </Col>
          <Col className={"col-auto collapse-icon-margin"}>
            {isOpen ? <FaAngleDown /> : <FaAngleRight />}
          </Col>
        </Row>
      </Card.Header>
      {isOpen && (
        <Card.Body className={"collapse-card-body"}>{props.children}</Card.Body>
      )}
    </Card>
  );
}

export default CollapseCard;
