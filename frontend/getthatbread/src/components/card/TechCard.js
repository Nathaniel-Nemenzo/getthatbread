import React, { Component } from "react";
import { Card } from "react-bootstrap";

import "./card.css";

class TechCard extends Component {
  render() {
    return (
      <a
        className={"no-link-decoration"}
        href={this.props.url}
        target={"_blank"}
        rel="noreferrer"
      >
        <Card className={"clickable-card"}>
          <Card.Img className={"tools-img"} src={this.props.image} />
          <Card.Body>
            <Card.Title>{this.props.title}</Card.Title>
            <Card.Text>{this.props.desc}</Card.Text>
          </Card.Body>
        </Card>
      </a>
    );
  }
}

export default TechCard;
