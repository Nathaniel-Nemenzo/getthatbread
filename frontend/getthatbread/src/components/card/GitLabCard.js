import React, { Component } from "react";
import { Card } from "react-bootstrap";

import "./card.css";

class GitLabCard extends Component {
  render() {
    return (
      <div className={"developer"}>
        <Card>
          <Card.Img src={this.props.image} />
          <Card.Body>
            <Card.Title>{this.props.name}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">
              {this.props.role}
            </Card.Subtitle>
            <Card.Text>{this.props.bio}</Card.Text>
          </Card.Body>
          <Card.Footer>
            <div className={"gitlab-stats"}>
              Commits: {this.props.commits} | Closed Issues: {this.props.issues}{" "}
              | Unit Tests: {this.props.tests}
            </div>
          </Card.Footer>
        </Card>
      </div>
    );
  }
}

export default GitLabCard;
