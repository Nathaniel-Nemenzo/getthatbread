import React from "react";
import { Card, Row, Col } from "react-bootstrap";

import {
  BsFillPersonCheckFill,
  BsBuilding,
  BsFillPinMapFill,
} from "react-icons/bs";

function getIcon(type: string) {
  switch (type) {
    case "Jobs":
      return <BsFillPersonCheckFill size={120} />;
    case "Companies":
      return <BsBuilding size={120} />;
    case "Cities":
      return <BsFillPinMapFill size={120} />;
    default:
      return <BsFillPersonCheckFill size={120} />;
  }
}

function getURL(type: string) {
  switch (type) {
    case "Jobs":
      return "/job";
    case "Companies":
      return "/company";
    case "Cities":
      return "/city";
    default:
      return "/";
  }
}

function ModelCard(props: { title: string }) {
  return (
    <a href={getURL(props.title)} className={"no-link-decoration"}>
      <Card className={"clickable-card"}>
        <Card.Body>
          <Row style={{ marginTop: "36px " }}>
            <Col>
              <h2>{props.title}</h2>
            </Col>
          </Row>
          <Row style={{ marginBottom: "36px " }}>
            <Col style={{ textAlign: "center" }}>{getIcon(props.title)}</Col>
          </Row>
        </Card.Body>
      </Card>
    </a>
  );
}

export default ModelCard;
