import React from "react";
import renderer from "react-test-renderer";

import GitLabCard from "./GitLabCard";
import TechCard from "./TechCard";
import CollapseCard from "./CollapseCard";
import ModelCard from "./ModelCard";

test("gitlab card properly renders", () => {
  const component = renderer.create(
    <GitLabCard
      image="test image"
      name="test name"
      role="test role"
      bio="test bio"
      commits="test commits"
      issues="test issues"
      tests="test tests"
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("tech card properly renders", () => {
  const component = renderer.create(
    <TechCard
      url="test url"
      image="test image"
      title="test title"
      desc="test desc"
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("collapse card properly renders", () => {
  const component = renderer.create(
    <CollapseCard title={"Test"}>
      <div>Test data</div>
    </CollapseCard>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test("model card properly renders", () => {
  const component = renderer.create(<ModelCard title="Jobs" />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
