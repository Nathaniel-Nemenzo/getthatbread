import React from "react";
import renderer from "react-test-renderer";

import ModelPagination from "./ModelPagination";

test("model pagination properly renders", () => {
  const component = renderer.create(
    <ModelPagination
      activePage={1}
      firstPage={1}
      lastPage={Math.ceil((100 * 1.0) / 10)}
      updatePage={(p) => {
        jest.fn();
      }}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
