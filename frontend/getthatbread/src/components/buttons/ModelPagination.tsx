import React from "react";
import { Pagination } from "react-bootstrap";

const MAX_PAGES = 5;

type PaginationProps = {
  activePage: number;
  firstPage: number;
  lastPage: number;
  updatePage: (...args: Array<number>) => any;
};

function displayPages(props: PaginationProps) {
  let pages = [];
  const borderPages = Math.floor(MAX_PAGES / 2);

  // edge case
  if (props.activePage - borderPages < props.firstPage) {
    let lastPage = MAX_PAGES;
    lastPage = lastPage > props.lastPage ? props.lastPage : lastPage;

    for (let i = 1; i <= lastPage; i++) {
      pages.push(
        <Pagination.Item
          key={i}
          active={props.activePage === i}
          onClick={() => {
            if (props.activePage !== i) props.updatePage(i);
          }}
        >
          {i}
        </Pagination.Item>
      );
    }
    // edge case
  } else if (props.activePage + borderPages > props.lastPage) {
    let startPage = props.lastPage - MAX_PAGES + 1;
    startPage = startPage <= 0 ? 1 : startPage;

    for (let i = startPage; i <= props.lastPage; i++) {
      pages.push(
        <Pagination.Item
          key={i}
          active={props.activePage === i}
          onClick={() => {
            if (props.activePage !== i) props.updatePage(i);
          }}
        >
          {i}
        </Pagination.Item>
      );
    }
  } else {
    let startPage = props.activePage - borderPages;
    startPage = startPage <= 0 ? 1 : startPage;
    let lastPage = props.activePage + borderPages;
    lastPage = lastPage > props.lastPage ? props.lastPage : lastPage;

    for (let i = startPage; i <= lastPage; i++) {
      pages.push(
        <Pagination.Item
          key={i}
          active={props.activePage === i}
          onClick={() => {
            if (props.activePage !== i) props.updatePage(i);
          }}
        >
          {i}
        </Pagination.Item>
      );
    }
  }

  return pages;
}

function ModelPagination(props: PaginationProps) {
  return (
    <Pagination className="d-flex justify-content-center">
      <Pagination.First
        onClick={() => {
          if (props.activePage !== props.firstPage)
            props.updatePage(props.firstPage);
        }}
      />

      <Pagination.Prev
        onClick={() => {
          if (props.activePage !== props.firstPage)
            props.updatePage(props.activePage - 1);
        }}
      />

      {displayPages(props)}

      <Pagination.Next
        onClick={() => {
          if (props.activePage !== props.lastPage)
            props.updatePage(props.activePage + 1);
        }}
      />

      <Pagination.Last
        onClick={() => {
          if (props.activePage !== props.lastPage)
            props.updatePage(props.lastPage);
        }}
      />
    </Pagination>
  );
}

export default ModelPagination;
