import React from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";

const MapWrapper = withScriptjs(
  withGoogleMap((props: { latitude: number; longitude: number }) => {
    const { latitude, longitude } = props;

    return (
      <GoogleMap
        defaultZoom={15}
        defaultCenter={{ lat: latitude, lng: longitude }}
      >
        <Marker position={{ lat: latitude, lng: longitude }} />
      </GoogleMap>
    );
  })
);

const Map = (props: { latitude: number; longitude: number }) => {
  const { latitude, longitude } = props;

  return (
    <MapWrapper
      latitude={latitude}
      longitude={longitude}
      loadingElement={<div style={{ height: `100%` }} />}
      googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhS7pYwEDyJFbUSXpdOGff_3P0suRxse8"
      containerElement={<div style={{ height: `400px` }} />}
      mapElement={<div style={{ height: `100%` }} />}
    />
  );
};

export default Map;
