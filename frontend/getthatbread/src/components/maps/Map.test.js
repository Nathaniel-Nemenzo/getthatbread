import React from "react";
import renderer from "react-test-renderer";

import Map from "./Map";

test("map properly renders", () => {
  const component = renderer.create(<Map latitude={0} longitude={0} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
