import React from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

import Map from "../../components/maps/Map";
import { checkUnknowns } from "../../library/Functions";

import "../../index.css";

function JobPage() {
  const { jobID } = useParams();
  const [job, setJob] = useState({
    id: jobID,
    name: "",
    icon: "",
    company_id: "",
    company_name: "",
    city_id: "",
    location: "",
    about: "",
    salary: "",
    job_type: "",
    link: "",
  });

  const [location, setLocation] = useState({
    latitude: undefined,
    longitude: undefined,
  });

  const [company, setCompany] = useState({
    about: "",
    logo: "",
    url: "",
  });

  useEffect(() => {
    axios
      .get(`https://api.getthatbread.me/api/jobs/${jobID}`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        const data = response["data"];

        const jobInstance = {
          id: jobID,
          name: data["jobTitle"],
          icon: "",
          company_id: data["companyId"],
          company_name: data["companyName"],
          city_id: data["cityId"],
          location: checkUnknowns(data["location"], "Unknown"),
          about: checkUnknowns(data["description"], "Unknown"),
          salary: checkUnknowns(data["salary"], "Unknown"),
          job_type: checkUnknowns(data["jobType"], "Unknown"),
          link: data["indeedFinalUrl"],
        };

        setJob(jobInstance);
      })
      .catch((error) => console.log(error));
  }, [jobID, job]);

  useEffect(() => {
    axios
      .get(`https://api.getthatbread.me/api/cities/${job?.city_id}`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        const data = response["data"];

        const location = {
          latitude: data["latitude"],
          longitude: data["longitude"],
        };

        setLocation(location);
      })
      .catch((error) => console.log(error));
  }, [job?.city_id]);

  useEffect(() => {
    axios
      .get(`https://api.getthatbread.me/api/companies/${job?.company_id}`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        const data = response["data"];

        const comp = {
          about: data["description"],
          logo: data["logoUrl"],
          url: data["indeedFinalUrl"],
        };

        setCompany(comp);
        console.log(comp);
      })
      .catch((error) => console.log(error));
  }, [job?.company_id]);

  if (jobID === undefined) {
    return <div>Job cannot be found!</div>;
  }

  return (
    <Container>
      <Row className="mt-4">
        <Col lg={8}>
          <Row>
            <Col className="mb-4">
              <Card>
                <Card.Body>
                  <Card.Title>{job?.name}</Card.Title>
                  <Card.Subtitle>ID: {job?.id}</Card.Subtitle>
                  <Card.Text>
                    <Row>
                      <Col>
                        <p
                          className={"instance-description"}
                          dangerouslySetInnerHTML={{ __html: job?.about }}
                        ></p>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <h3 className={"instance-h3"}>About the position</h3>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={2}>
                        <h4 className={"instance-h4"}>Job type</h4>
                        {job?.job_type}
                      </Col>
                      <Col lg={4}>
                        <h4 className={"instance-h4"}>Salary</h4>
                        {job?.salary === "" ? "Unknown" : job?.salary}
                      </Col>
                    </Row>

                    <br />
                    <br />
                    <br />

                    <Row>
                      <Col lg={12}>
                        <Button
                          variant="primary"
                          className={"instance-apply-button"}
                          href={job?.link}
                        >
                          Apply now
                        </Button>
                      </Col>
                    </Row>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col>
          <Row className={"half-height"}>
            <Card className={"half-height"}>
              <Card.Body>
                <Row style={{ marginBottom: "12px" }}>
                  <Col lg={3}>
                    <img
                      src={company?.logo}
                      alt={"company logo"}
                      width="100%"
                    />
                  </Col>
                  <Col lg={9} className={"instance-page-center"}>
                    <Card.Title>
                      <a
                        href={`/company/${job?.company_id}`}
                        className={"card-links"}
                      >
                        {job?.company_name}
                      </a>
                    </Card.Title>
                    <Card.Subtitle>Company</Card.Subtitle>
                  </Col>
                </Row>
                <Card.Text>{company?.about}</Card.Text>
              </Card.Body>
            </Card>
          </Row>
          <Row className={"mt-4 half-height"} style={{ marginBottom: "50px" }}>
            <Card className={"half-height"}>
              <Card.Body>
                <div style={{ marginBottom: "12px" }}>
                  <Card.Title>
                    <a href={`/city/${job?.city_id}`} className={"card-links"}>
                      {job?.location}
                    </a>
                  </Card.Title>
                  <Card.Subtitle>Location</Card.Subtitle>
                </div>
                <Card.Text>
                  {location?.latitude === undefined ||
                  location?.longitude === undefined ? (
                    <span>Location cannot be found</span>
                  ) : (
                    <Map
                      latitude={location?.latitude}
                      longitude={location?.longitude}
                    />
                  )}
                </Card.Text>
              </Card.Body>
            </Card>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default JobPage;
