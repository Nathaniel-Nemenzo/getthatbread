import React from "react";
import { useEffect, useState } from "react";
import axios from "axios";
import { Container, Row, Col } from "react-bootstrap";

import { JobInterface } from "../../interfaces";
import Filter from "../../components/filter/Filter";
import SearchGrid from "../../components/search/searchgrid/SearchGrid";
import ModelPagination from "../../components/buttons/ModelPagination";
import { checkUnknowns, getLogoLink } from "../../library/Functions";
import {
  MAX_INSTANCES_PER_PAGE,
  JOB_SORT_OPTIONS,
} from "../../library/Constants";

function JobSearch() {
  const [jobs, setJobs] = useState([] as JobInterface[]);
  const [currPage, setCurrPage] = useState(1);
  const [numJobs, setNumJobs] = useState(0);

  // for filtering
  const [filterName, setFilterName] = useState("");
  const [filterCompany, setFilterCompany] = useState("");
  const [filterLocation, setFilterLocation] = useState("");
  const [filterJobType, setFilterJobType] = useState("");
  const [onEnter, setOnEnter] = useState(0);

  // for sorting
  const [sort, setSort] = useState("");

  const constructURLParams = () => {
    var resultURL = "?";
    if (filterName.length !== 0) {
      resultURL += `job_title=${filterName}&`;
    }
    if (filterLocation.length !== 0) {
      resultURL += `location=${filterLocation}&`;
    }
    if (filterCompany.length !== 0) {
      resultURL += `company=${filterCompany}&`;
    }
    if (filterJobType.length !== 0) {
      resultURL += `job_type=${filterJobType}&`;
    }
    if (sort.length !== 0) {
      resultURL += `sort=${getSortingOption(sort)}&`;
    }
    return resultURL.substring(0, resultURL.length - 1);
  };

  const getSortingOption = (option: string) => {
    switch (option) {
      case "Name (A-Z)":
        return "job_title";
      case "Name (Z-A)":
        return "job_title^";
      case "Company (A-Z)":
        return "company";
      case "Company (Z-A)":
        return "company^";
      case "Salary (low to high)":
        return "salary_upper_bound";
      case "Salary (high to low)":
        return "salary_upper_bound^";
      default:
        return "";
    }
  };

  useEffect(() => {
    console.log(constructURLParams());
    axios
      .get(`https://api.getthatbread.me/api/jobs${constructURLParams()}`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        setNumJobs(response["data"]["count"]);
      })
      .catch((error) => console.log(error));
  }, [filterName, onEnter, sort]); // eslint-disable-line

  // grab a page of jobs
  useEffect(() => {
    var params = {
      offset: (currPage - 1) * MAX_INSTANCES_PER_PAGE,
      limit: MAX_INSTANCES_PER_PAGE,
    };
    console.log(`https://api.getthatbread.me/api/jobs${constructURLParams()}`);
    axios
      .get(`https://api.getthatbread.me/api/jobs${constructURLParams()}`, {
        headers: {
          "Content-Type": "application/json",
        },
        params: params,
      })
      .then((response) => {
        const result = response["data"]["data"];
        let jobs: JobInterface[] = [];

        for (let job of result) {
          let x: JobInterface = {
            type: "job",
            id: job["jobId"],
            title: job["jobTitle"],
            icon: `https://logo.clearbit.com/${getLogoLink(
              job["companyId"]
            )}.com`,
            subtitle: `${job["companyName"]} | ${
              job["location"]
            } | ${checkUnknowns(job["salary"], "Unknown")}`,
            keywords: [checkUnknowns(job["jobType"], "Unknown")],
            additional_info: [],
            description: [],

            // for formatting issues
            card_size: 4,
          };
          jobs.push(x);
        }
        setJobs(jobs);
      })
      .catch((error) => console.log(error));
  }, [currPage, filterName, onEnter, sort]); // eslint-disable-line

  return (
    <Container className="center">
      <Col lg={12}>
        <Row style={{ marginTop: "20px" }}>
          <Col>
            <Filter
              searchPlaceholder={"Search by job title"}
              onSearchChange={(e) => setFilterName(e)}
              onSubmitForm={() => setOnEnter(onEnter + 1)}
              sortOptions={JOB_SORT_OPTIONS}
              onSortChange={(e) => setSort(e)}
            >
              <div>
                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="company">Company</label>
                  </div>
                  <input
                    type="text"
                    name="company"
                    value={filterCompany}
                    className={"span-container-filter"}
                    onChange={(e) => setFilterCompany(e.target.value)}
                  />
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="location">Location</label>
                  </div>
                  <input
                    type="text"
                    name="location"
                    value={filterLocation}
                    className={"span-container-filter"}
                    onChange={(e) => setFilterLocation(e.target.value)}
                  />
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="job-type">Job type</label>
                  </div>
                  <Row>
                    <Col lg={4}>
                      <input
                        type="radio"
                        id="full-time"
                        name="job-type"
                        onChange={(e) => setFilterJobType(e.target.id)}
                        checked={filterJobType === "full-time"}
                      />
                      <label htmlFor="full-time" className={"checkbox-label"}>
                        Full-time
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="radio"
                        id="part-time"
                        name="job-type"
                        onChange={(e) => setFilterJobType(e.target.id)}
                        checked={filterJobType === "part-time"}
                      />
                      <label htmlFor="part-time" className={"checkbox-label"}>
                        Part-time
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="radio"
                        id="internship"
                        name="job-type"
                        onChange={(e) => setFilterJobType(e.target.id)}
                        checked={filterJobType === "internship"}
                      />
                      <label htmlFor="internship" className={"checkbox-label"}>
                        Internship
                      </label>
                    </Col>
                  </Row>
                </div>
              </div>
            </Filter>
          </Col>
        </Row>
        <Row style={{ marginBottom: "50px" }}>
          <Col>
            <SearchGrid
              data={jobs}
              searchQuery={filterName}
              pageNum={currPage}
              totalInstances={numJobs}
              pagination={
                <ModelPagination
                  activePage={currPage}
                  firstPage={1}
                  lastPage={Math.ceil((numJobs * 1.0) / MAX_INSTANCES_PER_PAGE)}
                  updatePage={(p) => {
                    setCurrPage(p);
                  }}
                />
              }
            />
          </Col>
        </Row>
      </Col>
    </Container>
  );
}

export default JobSearch;
