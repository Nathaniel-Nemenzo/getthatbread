import React from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

import Map from "../../components/maps/Map";
import SearchCard from "../../components/search/searchgrid/SearchCard";
import { JobInterface } from "../../interfaces";
import { Timeline } from "react-twitter-widgets";
import {
  checkUnknowns,
  getLogoLink,
  getStarRating,
} from "../../library/Functions";

import "../../index.css";

// function getBenefits(myBenefits: string[]) {
//   return COMPANY_BENEFITS.map((benefit) => {
//     const prefix = myBenefits.includes(benefit) ? "" : "un";
//     return (
//       <Col lg={4} className={"benefits-column"}>
//         <img
//           alt={"checkbox"}
//           src={`https://img.icons8.com/ios-filled/344/${prefix}checked-checkbox.png`}
//           className={"benefits-group"}
//         />
//         <div className={"benefits-group"}>{benefit}</div>
//       </Col>
//     );
//   });
// }

function CompanyPage() {
  const { companyID } = useParams();
  const [company, setCompany] = useState({
    id: companyID,
    name: "",
    icon: "",
    location: "",
    about: "",
    industry: "",
    number_of_employees: "",
    revenue: "",
    rating: 0,
    twitter: "",
    url: "",
    indeedUrl: "",
    benefits: [],
  });
  const [jobsAvail, setJobsAvail] = useState([] as JobInterface[]);
  const [location, setLocation] = useState({
    latitude: undefined,
    longitude: undefined,
    city_id: "",
  });

  useEffect(() => {
    axios
      .get(`https://api.getthatbread.me/api/companies/${companyID}`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        const data = response["data"];

        const compInstance = {
          id: data["companyId"],
          name: data["companyName"].replace(/\..*$/, ""),
          icon: data["logoUrl"],
          location: checkUnknowns(data["hqLocation"], "Unknown"),
          about: checkUnknowns(
            data["description"].replace(/(<([^>]+)>)/gi, ""),
            "Unknown"
          ),
          industry: checkUnknowns(data["sector"], "Unknown"),
          number_of_employees: checkUnknowns(data["employees"], "Unknown"),
          revenue: checkUnknowns(data["revenue"], "Unknown"),
          rating: data["rating"],
          twitter: data["twitterId"],
          url: data["url"],
          indeedUrl: data["indeedFinalUrl"],
          benefits: [],
        };

        setCompany(compInstance);
      })
      .catch((error) => console.log(error));
  }, [companyID]);

  useEffect(() => {
    let companyName = company?.name === "" ? "unknown" : company?.name;
    var params = { company: companyName };
    axios
      .get(`https://api.getthatbread.me/api/jobs`, {
        headers: {
          "Content-Type": "application/json",
        },
        params: params,
      })
      .then((response) => {
        const data = response["data"]["data"];
        let jobInstances: JobInterface[] = [];

        for (let job of data) {
          let x: JobInterface = {
            type: "job",
            id: job["jobId"],
            title: job["jobTitle"],
            icon: `https://logo.clearbit.com/${getLogoLink(
              job["companyName"]
            )}.com`,
            subtitle: `${job["companyName"]} | ${
              job["location"]
            } | ${checkUnknowns(job["salary"], "Unknown")}`,
            keywords: [checkUnknowns(job["jobType"], "Unknown")],
            additional_info: [],
            description: [],

            // for formatting issues
            card_size: 12,
          };
          jobInstances.push(x);
        }
        setJobsAvail(jobInstances);
      })
      .catch((error) => console.log(error));
  }, [company?.name]);

  useEffect(() => {
    if (company?.location !== undefined && company?.location.length !== 0) {
      let city = company?.location.replace(/,.*/, "");
      console.log("CITYYY", city);
      var params = { name: city, limit: 1 };
      axios
        .get(`https://api.getthatbread.me/api/cities`, {
          headers: {
            "Content-Type": "application/json",
          },
          params: params,
        })
        .then((response) => {
          const data = response["data"]["data"][0];
          console.log(data);

          const location = {
            latitude: data["latitude"],
            longitude: data["longitude"],
            city_id: data["cityId"],
          };

          setLocation(location);
        })
        .catch((error) => console.log(error));
    }
  }, [company?.location]);

  if (companyID === undefined) {
    return <div>Job cannot be found!</div>;
  }

  return (
    <Container>
      <Row className="mt-4" style={{ marginBottom: "30px" }}>
        <Col lg={8}>
          <Row>
            <Col className="mb-4">
              <Card>
                <Card.Body>
                  {/* Company title, description, and logo */}
                  <Row>
                    <Col lg={3}>
                      <img
                        alt="company icon"
                        src={company?.icon}
                        width={"100%"}
                      />
                    </Col>
                    <Col lg={9}>
                      <Card.Title>
                        <span className={"instance-page-title"}>
                          <a href={company?.url} className={"card-links"}>
                            {company?.name}
                          </a>
                        </span>
                      </Card.Title>
                      <Card.Subtitle>{company?.industry}</Card.Subtitle>
                      <Card.Text>
                        <p className={"instance-description"}>
                          {company?.about}
                        </p>
                      </Card.Text>
                    </Col>
                  </Row>

                  {/* Specific details about company */}
                  <Row>
                    <Col lg={3}>
                      <h4 className={"instance-h4"}>Number of employees</h4>
                      {company?.number_of_employees}
                    </Col>
                  </Row>

                  {/* Revenue and Ratings */}
                  <Row>
                    <Col lg={3}>
                      <h4 className={"instance-h4"}>Revenue</h4>
                      <span className={"capitalize"}>{company?.revenue}</span>
                    </Col>
                    <Col lg={3}>
                      <h4 className={"instance-h4"}>Rating</h4>
                      <div>
                        {getStarRating(company?.rating, 5)}{" "}
                        {`(${company?.rating})`}
                      </div>
                    </Col>
                  </Row>

                  <br />
                  <br />
                  <br />

                  {/* Display Indeed Button */}
                  <Row>
                    <Col lg={12}>
                      <Button
                        variant="primary"
                        className={"instance-apply-button"}
                        href={company?.indeedUrl}
                      >
                        View company profile
                      </Button>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card>
                <Card.Body>
                  <Card.Title>
                    <a
                      href={`/city/${location?.city_id}`}
                      className={"card-links"}
                    >
                      {company?.location}
                    </a>
                  </Card.Title>
                  <Card.Text>
                    {location?.latitude === undefined ||
                    location?.longitude === undefined ? (
                      <span>Location cannot be found</span>
                    ) : (
                      <Map
                        latitude={location?.latitude}
                        longitude={location?.longitude}
                      />
                      // {console.log()}
                    )}
                    {console.log(location?.latitude)}
                    {console.log(location?.longitude)}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Col>
        <Col lg={4}>
          <Row className={"half-height"}>
            <Col>
              <Card>
                <Card.Body className={"jobs-overflow"}>
                  <Card.Title>Job postings</Card.Title>
                  <Card.Text className={"jobs-overflow"}>
                    <Row>
                      {jobsAvail.map((item) => {
                        return <SearchCard data={item} />;
                      })}
                    </Row>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row className={"mt-4 half-height"}>
            <Col>
              <Card>
                <Card.Body>
                  <Timeline
                    dataSource={{
                      sourceType: "profile",
                      screenName: company?.twitter,
                    }}
                    options={{
                      height: "400",
                    }}
                  />
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default CompanyPage;
