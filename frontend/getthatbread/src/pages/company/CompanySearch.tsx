import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import axios from "axios";

import { CompanyInterface } from "../../interfaces";
import Filter from "../../components/filter/Filter";
import SearchGrid from "../../components/search/searchgrid/SearchGrid";
import ModelPagination from "../../components/buttons/ModelPagination";
import {
  MAX_INSTANCES_PER_PAGE,
  COMPANY_SECTORS,
  COMPANY_SORT_OPTIONS,
} from "../../library/Constants";
import { checkUnknowns } from "../../library/Functions";

function getSectors() {
  return COMPANY_SECTORS.map((item) => {
    return <option value={item} key={item} />;
  });
}

// call API to grab data for rendering
function CompanySearch() {
  const [companies, setCompanies] = useState([] as CompanyInterface[]);
  const [currPage, setCurrPage] = useState(1);
  const [numCompanies, setNumCompanies] = useState(0);

  // for filtering
  const [filterName, setFilterName] = useState("");
  const [filterSector, setFilterSector] = useState("");
  const [filterLocation, setFilterLocation] = useState("");
  const [filterEmployees, setFilterEmployees] = useState([] as number[]);
  const [filterRating, setFilterRating] = useState(-1);
  const [onEnter, setOnEnter] = useState(0);

  // for sorting
  const [sort, setSort] = useState("");

  const constructURLParams = () => {
    var resultURL = "?";
    if (filterName.length !== 0) {
      resultURL += `name=${filterName}&`;
    }
    if (filterLocation.length !== 0) {
      resultURL += `hq_location=${filterLocation}&`;
    }
    if (filterEmployees.length !== 0) {
      resultURL += `employees=${Math.max(...filterEmployees)}&`;
    }
    if (filterSector.length !== 0) {
      resultURL += `sector=${filterSector}&`;
    }
    if (filterRating !== -1) {
      resultURL += `rating=${filterRating}&`;
    }
    if (sort.length !== 0) {
      resultURL += `sort=${getSortingOption(sort)}&`;
    }
    return resultURL.substring(0, resultURL.length - 1);
  };

  const getSortingOption = (option: string) => {
    switch (option) {
      case "Name (A-Z)":
        return "name";
      case "Name (Z-A)":
        return "name^";
      case "Location (A-Z)":
        return "hq_location";
      case "Location (Z-A)":
        return "hq_location^";
      case "Sector (A-Z)":
        return "sector";
      case "Sector (Z-A)":
        return "sector^";
      case "Number of employees (low to high)":
        return "employees";
      case "Number of employees (high to low)":
        return "employees^";
      case "Rating (low to high)":
        return "rating";
      case "Rating (high to low)":
        return "rating^";
      default:
        return "";
    }
  };

  useEffect(() => {
    console.log(
      `https://api.getthatbread.me/api/companies${constructURLParams()}`
    );
    axios
      .get(`https://api.getthatbread.me/api/companies${constructURLParams()}`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        setNumCompanies(response["data"]["count"]);
      })
      .catch((error) => console.log(error));
  }, [filterName, onEnter, sort]); // eslint-disable-line

  useEffect(() => {
    var params = {
      offset: (currPage - 1) * MAX_INSTANCES_PER_PAGE,
      limit: MAX_INSTANCES_PER_PAGE,
    };
    axios
      .get(`https://api.getthatbread.me/api/companies${constructURLParams()}`, {
        headers: {
          "Content-Type": "application/json",
        },
        params: params,
      })
      .then((response) => {
        const result = response["data"]["data"];
        let companies: CompanyInterface[] = [];

        for (let comp of result) {
          let x: CompanyInterface = {
            type: "company",
            id: comp["companyId"],
            title: checkUnknowns(comp["companyName"], "Unknown").replace(
              /\..*$/,
              ""
            ),
            icon: comp["logoUrl"],
            subtitle: checkUnknowns(comp["hqLocation"], "Unknown"),
            keywords: [checkUnknowns(comp["sector"], "Unknown")],
            additional_info: [checkUnknowns(comp["employees"], "Unknown")],
            description: [
              `Number of employees: ${checkUnknowns(
                comp["employees"],
                "Unknown"
              )}`,
              `Rating: ${comp["rating"]}`,
            ],

            // for formatting issues
            card_size: 4,
          };
          companies.push(x);
        }

        setCompanies(companies);
      })
      .catch((error) => console.log(error));
  }, [currPage, filterName, onEnter, sort]); // eslint-disable-line

  return (
    <Container className="center">
      <Col lg={12}>
        <Row style={{ marginTop: "20px" }}>
          <Col>
            <Filter
              searchPlaceholder={"Search for a company"}
              onSearchChange={(e) => setFilterName(e)}
              onSubmitForm={() => setOnEnter(onEnter + 1)}
              sortOptions={COMPANY_SORT_OPTIONS}
              onSortChange={(e) => setSort(e)}
            >
              <div>
                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="location">Location</label>
                  </div>
                  <input
                    type="text"
                    name="location"
                    value={filterLocation}
                    className={"span-container-filter"}
                    onChange={(e) => setFilterLocation(e.target.value)}
                  />
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="size">Number of employees</label>
                  </div>
                  <Row>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="smallest-size"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEmploy = [...filterEmployees];
                            newEmploy.push(500);
                            setFilterEmployees(newEmploy);
                          } else {
                            var oldEmploy = [...filterEmployees];
                            setFilterEmployees(
                              oldEmploy.filter((item) => item !== 500)
                            );
                          }
                        }}
                        checked={filterEmployees.includes(500)}
                      />
                      <label
                        htmlFor="smallest-size"
                        className={"checkbox-label"}
                      >
                        1 to 500
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="small-size"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEmploy = [...filterEmployees];
                            newEmploy.push(1_000);
                            setFilterEmployees(newEmploy);
                          } else {
                            var oldEmploy = [...filterEmployees];
                            setFilterEmployees(
                              oldEmploy.filter((item) => item !== 1_000)
                            );
                          }
                        }}
                        checked={filterEmployees.includes(1_000)}
                      />
                      <label htmlFor="small-size" className={"checkbox-label"}>
                        501 to 1,000
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="med-size"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEmploy = [...filterEmployees];
                            newEmploy.push(5_000);
                            setFilterEmployees(newEmploy);
                          } else {
                            var oldEmploy = [...filterEmployees];
                            setFilterEmployees(
                              oldEmploy.filter((item) => item !== 5_000)
                            );
                          }
                        }}
                        checked={filterEmployees.includes(5_000)}
                      />
                      <label htmlFor="med-size" className={"checkbox-label"}>
                        1,001 to 5,000
                      </label>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="large-size"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEmploy = [...filterEmployees];
                            newEmploy.push(10_000);
                            setFilterEmployees(newEmploy);
                          } else {
                            var oldEmploy = [...filterEmployees];
                            setFilterEmployees(
                              oldEmploy.filter((item) => item !== 10_000)
                            );
                          }
                        }}
                        checked={filterEmployees.includes(10_000)}
                      />
                      <label htmlFor="large-size" className={"checkbox-label"}>
                        5,001 to 10,000
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="largest-size"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEmploy = [...filterEmployees];
                            newEmploy.push(10_000_000);
                            setFilterEmployees(newEmploy);
                          } else {
                            var oldEmploy = [...filterEmployees];
                            setFilterEmployees(
                              oldEmploy.filter((item) => item !== 10_000_000)
                            );
                          }
                        }}
                        checked={filterEmployees.includes(10_000_000)}
                      />
                      <label
                        htmlFor="largest-size"
                        className={"checkbox-label"}
                      >
                        10,000+
                      </label>
                    </Col>
                  </Row>
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="sector">Sector</label>
                  </div>
                  <input
                    list="sector"
                    name="sector"
                    value={filterSector}
                    className={"span-container-filter"}
                    onChange={(e) => setFilterSector(e.target.value)}
                  />
                  <datalist id="sector">{getSectors()}</datalist>
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="rating">Rating (1 to 5)</label>
                  </div>
                  <Row>
                    <Col>
                      <input
                        type="range"
                        min="1"
                        max="5"
                        value={filterRating}
                        step="0.1"
                        name="rating"
                        id="rating"
                        onChange={(e) =>
                          setFilterRating(parseFloat(e.target.value))
                        }
                      />
                    </Col>
                  </Row>
                </div>
              </div>
            </Filter>
          </Col>
        </Row>
        <Row style={{ marginBottom: "50px" }}>
          <Col>
            <SearchGrid
              data={companies}
              searchQuery={filterName}
              pageNum={currPage}
              totalInstances={numCompanies}
              pagination={
                <ModelPagination
                  activePage={currPage}
                  firstPage={1}
                  lastPage={Math.ceil(
                    (numCompanies * 1.0) / MAX_INSTANCES_PER_PAGE
                  )}
                  updatePage={(p) => {
                    setCurrPage(p);
                  }}
                />
              }
            />
          </Col>
        </Row>
      </Col>
    </Container>
  );
}

export default CompanySearch;
