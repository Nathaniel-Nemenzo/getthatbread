import React, { useEffect, useState } from "react";
import { useParams, Navigate } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import axios from "axios";

import CollapseCard from "../../components/card/CollapseCard";
import SearchCard from "../../components/search/searchgrid/SearchCard";
import SearchBar from "../../components/search/searchbar/SearchBar";
import {
  JobInterface,
  CompanyInterface,
  CityInterface,
} from "../../interfaces";
import {
  getLogoLink,
  checkUnknowns,
  formatNumbers,
} from "../../library/Functions";

import "../../index.css";

function SearchPage() {
  var { searchID } = useParams();
  searchID = searchID?.substring(1);

  const [jobs, setJobs] = useState([] as JobInterface[]);
  const [companies, setCompanies] = useState([] as CompanyInterface[]);
  const [cities, setCities] = useState([] as CityInterface[]);

  // for search function
  const [currSearch, setCurrSearch] = useState("");
  const [submit, setSubmit] = useState(false);

  useEffect(() => {
    // let jobParams = { query: searchID, limit: 18 };
    let jobParams = { job_title: searchID, limit: 18 };

    // GET JOBS USING SEARCH ID
    axios
      .get("https://api.getthatbread.me/api/jobs", {
        headers: {
          "Content-Type": "application/json",
        },
        params: jobParams,
      })
      .then((response) => {
        const result = response["data"]["data"];
        let jobs: JobInterface[] = [];

        for (let job of result) {
          let x: JobInterface = {
            type: "job",
            id: job["jobId"],
            title: job["jobTitle"],
            icon: `https://logo.clearbit.com/${getLogoLink(
              job["companyId"]
            )}.com`,
            subtitle: `${job["companyName"]} | ${
              job["location"]
            } | ${checkUnknowns(job["salary"], "Unknown")}`,
            keywords:
              job["jobType"] !== undefined
                ? [checkUnknowns(job["jobType"], "Unknown")]
                : [],
            additional_info: [],
            description: [],

            // for formatting issues
            card_size: 4,
          };
          jobs.push(x);
        }

        setJobs(jobs);
      })
      .catch((error) => console.log(error));

    // let companyParams = { query: searchID, limit: 18 };
    let companyParams = { name: searchID, limit: 18 };

    // GET COMPANIES USING SEARCH ID
    axios
      .get("https://api.getthatbread.me/api/companies", {
        headers: {
          "Content-Type": "application/json",
        },
        params: companyParams,
      })
      .then((response) => {
        const result = response["data"]["data"];
        let companies: CompanyInterface[] = [];

        for (let comp of result) {
          let x: CompanyInterface = {
            type: "company",
            id: comp["companyId"],
            title: checkUnknowns(comp["companyName"], "Unknown").replace(
              /\..*$/,
              ""
            ),
            icon: `https://logo.clearbit.com/${getLogoLink(
              comp["companyName"]
            )}.com`,
            subtitle: checkUnknowns(comp["hqLocation"], "Unknown"),
            keywords: [checkUnknowns(comp["sector"], "Unknown")],
            additional_info: [checkUnknowns(comp["employees"], "Unknown")],
            description: [
              `Number of employees: ${checkUnknowns(
                comp["employees"],
                "Unknown"
              )}`,
              `Rating: ${comp["rating"]}`,
            ],

            // for formatting issues
            card_size: 4,
          };
          companies.push(x);
        }

        setCompanies(companies);
      })
      .catch((error) => console.log(error));

    // let cityParams = { query: searchID, limit: 18 };
    let cityParams = { name: searchID, limit: 18 };

    // GET CITIES USING SEARCH ID
    axios
      .get("https://api.getthatbread.me/api/cities", {
        headers: {
          "Content-Type": "application/json",
        },
        params: cityParams,
      })
      .then((response) => {
        const result = response["data"]["data"];
        let cities: CityInterface[] = [];

        for (let city of result) {
          let x: CityInterface = {
            type: "city",
            id: city["cityId"],
            title: checkUnknowns(city["cityName"], "Unknown").replace(
              /\..*$/,
              ""
            ),
            icon: `https://countryflagsapi.com/svg/USA`,
            subtitle: `${checkUnknowns(
              city["state"],
              "Unknown"
            )} | ${checkUnknowns(city["country"], "Unknown")}`,
            keywords: [],
            additional_info: [
              checkUnknowns(city["population"].toString(), "Unknown"),
            ],
            description: [
              `Population: ${formatNumbers(city["population"])}`,
              `Average Rating: ${city["average_rating"]?.toFixed(2)}`,
              `Budget Rating: ${city["budget"]} | Safety Rating: ${city["safety"]}`,
            ],

            city: city["cityName"],
            state: city["state"],
            country: city["country"],
            country_code: "USA",

            // for formatting issues
            card_size: 4,
          };
          cities.push(x);
        }

        setCities(cities);
      })
      .catch((error) => console.log(error));

    setSubmit(false); // avoid infinite loop on refresh
  }, [searchID]);

  if (submit) {
    return <Navigate to={`/search&${currSearch}`} />;
  } else {
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <h1 className={"search-title"}>
                {searchID?.length === 0
                  ? `Start a new search.`
                  : `Search results for "${searchID}"`}
              </h1>
            </Col>
          </Row>

          {/* Make a new search */}
          <Row style={{ marginBottom: "50px" }}>
            <Col>
              <SearchBar
                placeholder={"Make a new search"}
                onChange={(e) => setCurrSearch(e)}
                onEnter={() => setSubmit(true)}
              />
            </Col>
          </Row>

          {/* Displays job posting results */}
          <Row style={{ marginBottom: "25px" }}>
            <Col>
              <CollapseCard title={"Jobs"}>
                <div>
                  <Row className="" xs={1} md={2}>
                    {jobs.length === 0 ? (
                      <div>No jobs found.</div>
                    ) : (
                      jobs.map((item, index) => {
                        return (
                          <SearchCard
                            data={item}
                            key={index}
                            searchQuery={searchID}
                          />
                        );
                      })
                    )}
                  </Row>
                  <Row>
                    <Col>
                      <a href="/job">See all job postings.</a>
                    </Col>
                  </Row>
                </div>
              </CollapseCard>
            </Col>
          </Row>

          {/* Displays company results */}
          <Row style={{ marginBottom: "25px" }}>
            <Col>
              <CollapseCard title={"Companies"}>
                <div>
                  <Row className="" xs={1} md={2}>
                    {companies.length === 0 ? (
                      <div>No companies found.</div>
                    ) : (
                      companies.map((item, index) => {
                        return (
                          <SearchCard
                            data={item}
                            key={index}
                            searchQuery={searchID}
                          />
                        );
                      })
                    )}
                  </Row>
                  <Row>
                    <Col>
                      <a href="/company">See all companies.</a>
                    </Col>
                  </Row>
                </div>
              </CollapseCard>
            </Col>
          </Row>

          {/* Displays city results */}
          <Row style={{ marginBottom: "50px" }}>
            <Col>
              <CollapseCard title={"Cities"}>
                <div>
                  <Row className="" xs={1} md={2}>
                    {cities.length === 0 ? (
                      <div>No cities found.</div>
                    ) : (
                      cities.map((item, index) => {
                        return (
                          <SearchCard
                            data={item}
                            key={index}
                            searchQuery={searchID}
                          />
                        );
                      })
                    )}
                  </Row>
                  <Row>
                    <Col>
                      <a href="/city">See all cities.</a>
                    </Col>
                  </Row>
                </div>
              </CollapseCard>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default SearchPage;
