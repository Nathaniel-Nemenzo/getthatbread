import React, { Component } from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import axios from "axios";

import GitLabCard from "../../components/card/GitLabCard";
import TechCard from "../../components/card/TechCard";

// import tools images
import aws from "../../assets/tools/aws.png";
import figma from "../../assets/tools/figma.png";
import flask from "../../assets/tools/flask.jpeg";
import gitlab from "../../assets/tools/gitlab.svg";
import postman from "../../assets/tools/postman.png";
import rds from "../../assets/tools/rds.png";
import react from "../../assets/tools/react.png";
import reactBootstrap from "../../assets/tools/react-bootstrap.png";

// import api images
import clearbit from "../../assets/apis/clearbit.png";
import crunchbase from "../../assets/apis/crunchbase.png";
import geodbCities from "../../assets/apis/geodb-cities.png";
import googleMaps from "../../assets/apis/google-maps.png";
import indeed from "../../assets/apis/indeed.png";
// import newsAPI from "../../assets/apis/news-api.png";
// import reddit from "../../assets/apis/reddit.png";
import twitter from "../../assets/apis/twitter.png";
// import youtube from "../../assets/apis/youtube.png";
import ziprecruiter from "../../assets/apis/ziprecruiter.png";

// import members images
import abinith from "../../assets/members/abinith.jpg";
import hamza from "../../assets/members/hamza.jpeg";
import lilly from "../../assets/members/lilly.jpg";
import nathaniel from "../../assets/members/nathaniel.jpeg";
import yifan from "../../assets/members/yifan.png";

var totalCommits = 0;
var totalIssues = 0;
var totalTests = 0;

class AboutPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      members: [
        {
          name: "Abinith Thallam",
          username: ["abinith-thallam", "abinith.thallam"],
          role: "Full-Stack | Phase 4 Leader",
          bio: "Hi! I'm a third-year CS major here at UT Austin. In my free time, I love hanging out with friends, trying new food, and playing tennis.",
          image: abinith,
          commits: 0,
          issues: 0,
          tests: 4,
        },
        {
          name: "Hamza Ali",
          username: ["hamzaali1", "Hamza Ali"],
          role: "Backend",
          bio: "I’m a 3rd year CS here at UT Austin. When I’m not drowning in school work, I enjoy reading books and sleeping.",
          image: hamza,
          commits: 0,
          issues: 0,
          tests: 23,
        },
        {
          name: "Lilly Tian",
          username: ["lillytian", "Lilly Tian"],
          role: "Frontend | Phase 3 Leader",
          bio: "I am a third-year CS major at UT Austin! I love to travel, watch movies, and look at pictures of my dog.",
          image: lilly,
          commits: 0,
          issues: 0,
          tests: 4,
        },
        {
          name: "Nathaniel Nemenzo",
          username: ["Nathaniel-Nemenzo", "nnemenzo9"],
          role: "Full-Stack | Phase 1 Leader",
          bio: "I am a junior in CS at UT Austin. I love going out with friends and family, bundling up in cold weather, and live renditions of hip-hop!",
          image: nathaniel,
          commits: 0,
          issues: 0,
          tests: 19,
        },
        {
          name: "Yifan Zhou",
          username: ["evanyfzhou", "yifanzhou1", "Yifan Zhou"],
          role: "Backend | Phase 2 Leader",
          bio: "Hello my name is Yifan, a UT student. I usually go cycling when I'm free and I also enjoy hiking.",
          image: yifan,
          commits: 0,
          issues: 0,
          tests: 21,
        },
      ],
    };

    this.findMember = this.findMember.bind(this);
    this.displayGitLabStats = this.displayGitLabStats.bind(this);
    this.displayGitLabCard = this.displayGitLabCard.bind(this);
  }

  // call before anything gets rendered
  componentDidMount() {
    // grabbing the number of commits
    axios
      .get(
        "https://gitlab.com/api/v4/projects/33875851/repository/contributors"
      )
      .then((res) => {
        // sort alphabetically based on name
        const rawData = res.data.sort((a, b) => {
          return a["name"].localeCompare(b["name"]);
        });

        // update members info for commits
        var oldMembers = this.state.members;
        rawData.forEach((member) => {
          var memberIndex = this.findMember(member["name"]);

          if (memberIndex !== -1) {
            oldMembers[memberIndex]["commits"] += member["commits"];
            totalCommits += member["commits"];
          }
        });

        this.setState({ members: oldMembers });
      });

    // grabbing the number of closed issues
    this.state.members.forEach((member, index) => {
      member["username"].forEach((id) => {
        axios
          .get(
            "https://gitlab.com/api/v4/projects/33875851/issues_statistics?assignee_username=" +
              id
          )
          .then((res) => {
            var issuesData = res.data;

            var oldMembers = this.state.members;
            oldMembers[index]["issues"] +=
              issuesData["statistics"]["counts"]["closed"];
            totalIssues += issuesData["statistics"]["counts"]["closed"];

            this.setState({ members: oldMembers });
          });
      });
    });

    // grabbing the number of unit tests
    this.state.members.forEach((member) => {
      totalTests += member["tests"];
    });
  }

  // returns index of the new member
  findMember = (name) => {
    for (var index = 0; index < this.state.members.length; index++) {
      var member = this.state.members[index];
      if (member["username"].includes(name)) {
        return index;
      }
    }
    return -1;
  };

  // returns a card for each team member and their stats
  displayGitLabStats = () => {
    const pairedData = this.state.members.reduce((rows, key, index) => {
      return (
        (index % 3 === 0
          ? rows.push([key])
          : rows[rows.length - 1].push(key)) && rows
      );
    }, []);

    return pairedData.map((rows) => {
      return (
        <Row className={"gitlab-members-row"}>
          {this.displayGitLabCard(rows)}
        </Row>
      );
    });
  };

  displayGitLabCard = (row) => {
    return row.map((member) => {
      return (
        <Col key={member["username"]} lg={4} md={6}>
          <GitLabCard
            name={member["name"]}
            username={"@" + member["username"]}
            role={member["role"]}
            bio={member["bio"]}
            image={member["image"]}
            commits={member["commits"]}
            issues={member["issues"]}
            tests={member["tests"]}
          />
        </Col>
      );
    });
  };

  render() {
    return (
      <div>
        <Container style={{ marginTop: "36px", marginBottom: "36px" }}>
          {/* Display team info */}
          <Row>
            <Col>
              <h1 className={"main-title"}>About Us</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            <Col md={10} lg={10}>
              <p className={"intro"}>
                GetThatBread is a job search application that empowers job
                searchers by providing them with quality information about the
                job itself and about life outside of the job, highlighting both
                the cities and the companies these jobs are from. Our mission is
                to enable users to find the perfect job for them.
              </p>
              <p className={"intro"}>
                This website will allow users to consider the company and city
                it will take place in while while applying for a job.
                Furthermore, a user can narrow down our list of available jobs
                by the professional requirements, by company, by industry, and
                more! If a user wants to first find companies that fit their
                lifestyle and culture, they can look through companies that
                currently have job postings, filtering by the size of the
                company, its industry, and the benefits it provides. Lastly, if
                a user desires to live in a certain city and find a job based in
                that city, they can search through cities that fit their desired
                lifestyle and find jobs and companies located there.
              </p>
            </Col>
          </Row>

          {/* Display each member's stats */}
          <Row className={"justify-content-md-center"}>
            <Col lg={10}>{this.displayGitLabStats()}</Col>
          </Row>

          {/* Display all repo stats */}
          <Row>
            <Col>
              <h1>Total Statistics</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            <Col lg={10}>
              <Row>
                <Col className={"tech-rows"} lg={4} md={6}>
                  <Card className={"repo-stat-card"}>
                    <Card.Body>
                      <Card.Title className={"repo-stat-titles"}>
                        Commits
                      </Card.Title>
                      <Card.Text className={"repo-stat-count"}>
                        {totalCommits}
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
                <Col className={"tech-rows"} lg={4} md={6}>
                  <Card className={"repo-stat-card"}>
                    <Card.Body>
                      <Card.Title className={"repo-stat-titles"}>
                        Issues
                      </Card.Title>
                      <Card.Text className={"repo-stat-count"}>
                        {totalIssues}
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
                <Col className={"tech-rows"} lg={4} md={6}>
                  <Card className={"repo-stat-card"}>
                    <Card.Body>
                      <Card.Title className={"repo-stat-titles"}>
                        Unit Tests
                      </Card.Title>
                      <Card.Text className={"repo-stat-count"}>
                        {totalTests}
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>

          {/* Display documentation */}
          <Row>
            <Col>
              <h1>Our Documentation</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            <Col lg={10}>
              <Row>
                <Col className={"tech-rows"} lg={4} md={6}>
                  <TechCard
                    image={gitlab}
                    url={"https://gitlab.com/Nathaniel-Nemenzo/getthatbread"}
                    title={"GitLab"}
                    desc={"Git Repository"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={4} md={6}>
                  <TechCard
                    image={postman}
                    url={
                      "https://documenter.getpostman.com/view/19775531/Uyr4LLQC"
                    }
                    title={"Postman"}
                    desc={"API Testing and Documentation"}
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          {/* Display technologies */}
          <Row>
            <Col>
              <h1>Our Technologies</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            <Col lg={10}>
              <Row>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={react}
                    url={"https://reactjs.org/"}
                    title={"React"}
                    desc={"Frontend Framework"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={reactBootstrap}
                    url={"https://react-bootstrap.github.io/"}
                    title={"React Bootstrap"}
                    desc={"Components Library"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={flask}
                    url={"https://flask.palletsprojects.com/en/2.0.x/"}
                    title={"Flask"}
                    desc={"Web Framework"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={aws}
                    url={"https://aws.amazon.com/"}
                    title={"AWS"}
                    desc={"Website Hosting"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={figma}
                    url={"https://figma.com/"}
                    title={"Figma"}
                    desc={"Design Tool"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={rds}
                    url={"https://aws.amazon.com/rds/"}
                    title={"Amazon RDS"}
                    desc={"Database"}
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          {/* Display our data sources */}
          <Row>
            <Col>
              <h1>Our APIs</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            <Col lg={10}>
              <Row>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={indeed}
                    url={"https://developer.indeed.com/"}
                    title={"Indeed"}
                    desc={"For Job Postings"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={googleMaps}
                    url={"https://developers.google.com/maps"}
                    title={"Google Maps"}
                    desc={"For Map Displays"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={ziprecruiter}
                    url={"https://www.ziprecruiter.com/partner"}
                    title={"ZipRecruiter"}
                    desc={"For Job Postings"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={crunchbase}
                    url={"https://data.crunchbase.com/docs/using-the-api"}
                    title={"Crunchbase"}
                    desc={"For Company Profiles"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={twitter}
                    url={"https://developer.twitter.com/en/docs/twitter-api"}
                    title={"Twitter"}
                    desc={"For Tweets"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={geodbCities}
                    url={
                      "https://rapidapi.com/wirefreethought/api/geodb-cities"
                    }
                    title={"GeoDB Cities"}
                    desc={"For Cities"}
                  />
                </Col>
                <Col className={"tech-rows"} lg={3} md={4}>
                  <TechCard
                    image={clearbit}
                    url={"https://clearbit.com/logo"}
                    title={"Clearbit"}
                    desc={"For Logos"}
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row>
            <Col>
              <h1>Interesting Trend from Integrating Data</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            <Col lg={10}>
              <p>
                An interesting trend we discovered was actually the lack of
                correlation between a city's size and it's COVID-19 levels.
                Generally one would assume that if there is a higher population,
                more people are likely to get infected. However, this does not
                seem to be the case. Perhaps what's more impactful in
                determining COVID-19 levels is the policy each city has to
                contain the spread of COVID-19.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default AboutPage;
