import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

import Hero from "./Hero.jsx";
import Footer from "../../components/footer/footer.jsx";
import ModelCard from "../../components/card/ModelCard";

class SplashPage extends Component {
  render() {
    return (
      <>
        <Hero />
        <Container>
          <Row
            className={"justify-content-md-center"}
            style={{ marginTop: "100px", marginBottom: "48px" }}
          >
            <Col lg={8}>
              <span className={"mission"}>
                Search for your next job by title, company, or city.
              </span>
            </Col>
          </Row>
          <Row
            className={"justify-content-md-center"}
            style={{ marginTop: "48px", marginBottom: "100px" }}
          >
            <Col lg={8}>
              <Row>
                <Col>
                  <ModelCard title={"Jobs"} />
                </Col>
                <Col>
                  <ModelCard title={"Companies"} />
                </Col>
                <Col>
                  <ModelCard title={"Cities"} />
                </Col>
              </Row>
            </Col>
          </Row>
          <Row
            className={"justify-content-md-center"}
            style={{ marginTop: "48px", marginBottom: "100px" }}
          >
            <Col lg={8}>
              <iframe
                width="100%"
                height="480px"
                src="https://www.youtube.com/embed/fEXAKzdD6Oc"
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </Col>
          </Row>
        </Container>

        <Footer />
      </>
    );
  }
}

export default SplashPage;
