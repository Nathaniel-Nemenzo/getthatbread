import React, { Component } from "react";
import ReactPlayer from "react-player";

import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { Navigate } from "react-router-dom";
import { Row, Col } from "react-bootstrap";

import heroVideo from "../../assets/splash_screen.mp4";
import "./Hero.css";
import SearchBar from "../../components/search/searchbar/SearchBar";

import { Parallax } from "react-scroll-parallax";

var styles = {
  root: {
    width: "100%",
    height: "100%",
    position: "relative",
    "& video": {
      objectFit: "cover",
    },
    top: 0,
    left: 0,
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "99%",
    backgroundColor: "rgba(0, 0, 0, 0.3)",
  },
};

class Hero extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currSearch: "",
      submit: false,
    };
  }

  render() {
    return this.state.submit ? (
      <Navigate to={`/search&${this.state.currSearch}`} />
    ) : (
      <section style={styles.root}>
        <ReactPlayer
          url={heroVideo}
          playing
          loop
          muted
          width="100%"
          height="100%"
        />
        <div style={styles.overlay}>
          <Box
            height="75%"
            display="flex"
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            color="#fff"
          >
            <Parallax speed={-15}>
              <Typography variant="h1" component="h1">
                <strong>find the perfect job for you.</strong>
              </Typography>
              <div style={styles.search}>
                <Row className="justify-content-center">
                  <Col lg={6}>
                    <SearchBar
                      placeholder="Begin your search here"
                      onChange={(e) => this.setState({ currSearch: e })}
                      onEnter={() => this.setState({ submit: true })}
                    />
                  </Col>
                </Row>
              </div>
            </Parallax>
          </Box>
        </div>
      </section>
    );
  }
}

export default Hero;
