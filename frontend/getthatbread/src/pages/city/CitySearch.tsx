import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import axios from "axios";

import Filter from "../../components/filter/Filter";
import SearchGrid from "../../components/search/searchgrid/SearchGrid";
import { CityInterface } from "../../interfaces";
import ModelPagination from "../../components/buttons/ModelPagination";
import { checkUnknowns, formatNumbers } from "../../library/Functions";
import {
  MAX_INSTANCES_PER_PAGE,
  CITY_SORT_OPTIONS,
} from "../../library/Constants";

import "../../index.css";

function CitySearch() {
  const [cities, setCities] = useState([] as CityInterface[]);
  const [currPage, setCurrPage] = useState(1);
  const [numCities, setNumCities] = useState(0);

  // for filtering
  const [filterName, setFilterName] = useState("");
  const [filterState, setFilterState] = useState("");
  const [filterCountry, setFilterCountry] = useState("");
  const [filterPopulation, setFilterPopulation] = useState([] as number[]);
  const [filterElevation, setFilterElevation] = useState([] as number[]);
  const [filterRating, setFilterRating] = useState(-1);
  const [filterSafety, setFilterSafety] = useState(-1);
  const [filterCOVID19, setFilterCOVID19] = useState([] as number[]);
  const [onEnter, setOnEnter] = useState(0);

  // for sorting
  const [sort, setSort] = useState("");

  const constructURLParams = () => {
    var resultURL = "?";
    if (filterName.length !== 0) {
      resultURL += `name=${filterName}&`;
    }
    if (filterState.length !== 0) {
      resultURL += `state=${filterState}&`;
    }
    if (filterCountry.length !== 0) {
      resultURL += `country=${filterCountry}&`;
    }
    if (filterPopulation.length !== 0) {
      resultURL += `population=${Math.max(...filterPopulation)}&`;
    }
    if (filterElevation.length !== 0) {
      resultURL += `elevation_meters=${Math.max(...filterElevation)}&`;
    }
    if (filterRating !== -1) {
      resultURL += `average_rating=${filterRating}&`;
    }
    if (filterSafety !== -1) {
      resultURL += `safety=${filterSafety}&`;
    }
    if (filterCOVID19.length !== 0) {
      resultURL += `covid=${Math.max(...filterCOVID19)}&`;
    }
    if (sort.length !== 0) {
      resultURL += `sort=${getSortingOption(sort)}&`;
    }
    return resultURL.substring(0, resultURL.length - 1);
  };

  const getSortingOption = (option: string) => {
    switch (option) {
      case "Name (A-Z)":
        return "name";
      case "Name (Z-A)":
        return "name^";
      case "Budget Rating (low to high)":
        return "budget";
      case "Budget Rating (high to low)":
        return "budget^";
      case "Safety Rating (low to high)":
        return "safety";
      case "Safety Rating (high to low)":
        return "safety^";
      case "COVID-19 Levels (low to high)":
        return "covid";
      case "COVID-19 Levels (high to low)":
        return "covid^";
      case "Rating (low to high)":
        return "average_rating";
      case "Rating (high to low)":
        return "average_rating^";
      default:
        return "";
    }
  };

  useEffect(() => {
    axios
      .get(`https://api.getthatbread.me/api/cities${constructURLParams()}`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        setNumCities(response["data"]["count"]);
      })
      .catch((error) => console.log(error));
  }, [filterName, onEnter, sort]); // eslint-disable-line

  useEffect(() => {
    var params = {
      offset: (currPage - 1) * MAX_INSTANCES_PER_PAGE,
      limit: MAX_INSTANCES_PER_PAGE,
    };
    axios
      .get(`https://api.getthatbread.me/api/cities${constructURLParams()}`, {
        headers: {
          "Content-Type": "application/json",
        },
        params: params,
      })
      .then((response) => {
        const result = response["data"]["data"];
        let cities: CityInterface[] = [];

        for (let city of result) {
          let x: CityInterface = {
            type: "city",
            id: city["cityId"],
            title: checkUnknowns(city["cityName"], "Unknown").replace(
              /\..*$/,
              ""
            ),
            icon: `https://countryflagsapi.com/svg/USA`,
            subtitle: `${checkUnknowns(
              city["state"],
              "Unknown"
            )}, ${checkUnknowns(city["country"], "Unknown")}`,
            keywords: [],
            additional_info: [
              checkUnknowns(city["population"].toString(), "Unknown"),
            ],
            description: [
              `Population: ${formatNumbers(city["population"])}`,
              `Average Rating: ${city["average_rating"]?.toFixed(2)}`,
              `Budget Rating: ${city["budget"]} | Safety Rating: ${city["safety"]}`,
            ],

            city: city["cityName"],
            state: city["cityId"].substring(city["cityId"].indexOf(",") + 1),
            country: city["country"],
            country_code: "USA",

            // for formatting issues
            card_size: 4,
          };
          cities.push(x);
        }

        setCities(cities);
      })
      .catch((error) => console.log(error));
  }, [currPage, filterName, onEnter, sort]); // eslint-disable-line

  return (
    <Container className="center">
      <Col lg={12}>
        <Row style={{ marginTop: "20px" }}>
          <Col>
            <Filter
              searchPlaceholder={"Search for a city"}
              onSearchChange={(e) => setFilterName(e)}
              onSubmitForm={() => setOnEnter(onEnter + 1)}
              sortOptions={CITY_SORT_OPTIONS}
              onSortChange={(e) => setSort(e)}
            >
              <div>
                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="state">State</label>
                  </div>
                  <input
                    type="text"
                    name="state"
                    className={"span-container-filter"}
                    value={filterState}
                    onChange={(e) => setFilterState(e.target.value)}
                  />
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="country">Country</label>
                  </div>
                  <input
                    type="text"
                    name="country"
                    className={"span-container-filter"}
                    value={filterCountry}
                    onChange={(e) => setFilterCountry(e.target.value)}
                  />
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="population">Population</label>
                  </div>
                  <Row>
                    <Col lg={6}>
                      <input
                        type="checkbox"
                        id="small-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newPop = [...filterPopulation];
                            newPop.push(100_000);
                            setFilterPopulation(newPop);
                          } else {
                            var oldPop = [...filterPopulation];
                            setFilterPopulation(
                              oldPop.filter((item) => item !== 100_000)
                            );
                          }
                        }}
                        checked={filterPopulation.includes(100_000)}
                      />
                      <label htmlFor="small-city" className={"checkbox-label"}>
                        50,000 to 99,999
                      </label>
                    </Col>
                    <Col lg={6}>
                      <input
                        type="checkbox"
                        id="small-med-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newPop = [...filterPopulation];
                            newPop.push(150_000);
                            setFilterPopulation(newPop);
                          } else {
                            var oldPop = [...filterPopulation];
                            setFilterPopulation(
                              oldPop.filter((item) => item !== 150_000)
                            );
                          }
                        }}
                        checked={filterPopulation.includes(150_000)}
                      />
                      <label
                        htmlFor="small-med-city"
                        className={"checkbox-label"}
                      >
                        100,000 to 149,999
                      </label>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={6}>
                      <input
                        type="checkbox"
                        id="med-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newPop = [...filterPopulation];
                            newPop.push(500_000);
                            setFilterPopulation(newPop);
                          } else {
                            var oldPop = [...filterPopulation];
                            setFilterPopulation(
                              oldPop.filter((item) => item !== 500_000)
                            );
                          }
                        }}
                        checked={filterPopulation.includes(500_000)}
                      />
                      <label htmlFor="med-city" className={"checkbox-label"}>
                        150,000 to 499,999
                      </label>
                    </Col>
                    <Col lg={6}>
                      <input
                        type="checkbox"
                        id="large-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newPop = [...filterPopulation];
                            newPop.push(10_000_000);
                            setFilterPopulation(newPop);
                          } else {
                            var oldPop = [...filterPopulation];
                            setFilterPopulation(
                              oldPop.filter((item) => item !== 10_000_000)
                            );
                          }
                        }}
                        checked={filterPopulation.includes(10_000_000)}
                      />
                      <label htmlFor="large-city" className={"checkbox-label"}>
                        500,000+
                      </label>
                    </Col>
                  </Row>
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="elevation">Elevation (in meters)</label>
                  </div>
                  <Row>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="lowest-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEle = [...filterElevation];
                            newEle.push(50);
                            setFilterElevation(newEle);
                          } else {
                            var oldEle = [...filterElevation];
                            setFilterElevation(
                              oldEle.filter((item) => item !== 50)
                            );
                          }
                        }}
                        checked={filterElevation.includes(50)}
                      />
                      <label htmlFor="lowest-city" className={"checkbox-label"}>
                        1 to 50
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="low-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEle = [...filterElevation];
                            newEle.push(100);
                            setFilterElevation(newEle);
                          } else {
                            var oldEle = [...filterElevation];
                            setFilterElevation(
                              oldEle.filter((item) => item !== 100)
                            );
                          }
                        }}
                        checked={filterElevation.includes(100)}
                      />
                      <label htmlFor="low-city" className={"checkbox-label"}>
                        50 to 100
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="low-med-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEle = [...filterElevation];
                            newEle.push(500);
                            setFilterElevation(newEle);
                          } else {
                            var oldEle = [...filterElevation];
                            setFilterElevation(
                              oldEle.filter((item) => item !== 500)
                            );
                          }
                        }}
                        checked={filterElevation.includes(500)}
                      />
                      <label
                        htmlFor="low-med-city"
                        className={"checkbox-label"}
                      >
                        101 to 500
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="med-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEle = [...filterElevation];
                            newEle.push(1_000);
                            setFilterElevation(newEle);
                          } else {
                            var oldEle = [...filterElevation];
                            setFilterElevation(
                              oldEle.filter((item) => item !== 1_000)
                            );
                          }
                        }}
                        checked={filterElevation.includes(1_000)}
                      />
                      <label htmlFor="med-city" className={"checkbox-label"}>
                        500 to 1,000
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="high-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEle = [...filterElevation];
                            newEle.push(5_000);
                            setFilterElevation(newEle);
                          } else {
                            var oldEle = [...filterElevation];
                            setFilterElevation(
                              oldEle.filter((item) => item !== 5_000)
                            );
                          }
                        }}
                        checked={filterElevation.includes(5_000)}
                      />
                      <label htmlFor="high-city" className={"checkbox-label"}>
                        1,001 to 5,000
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="highest-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newEle = [...filterElevation];
                            newEle.push(10_000_000);
                            setFilterElevation(newEle);
                          } else {
                            var oldEle = [...filterElevation];
                            setFilterElevation(
                              oldEle.filter((item) => item !== 10_000_000)
                            );
                          }
                        }}
                        checked={filterElevation.includes(10_000_000)}
                      />
                      <label
                        htmlFor="highest-city"
                        className={"checkbox-label"}
                      >
                        5,000+
                      </label>
                    </Col>
                  </Row>
                </div>

                <div className={"filter-group"}>
                  <Row>
                    <Col lg={6}>
                      <div className={"filter-group-title"}>
                        <label htmlFor="rating">Average Rating (1 to 5)</label>
                      </div>
                      <input
                        type="range"
                        min="1"
                        max="5"
                        value={filterRating}
                        step="0.1"
                        name="rating"
                        id="rating"
                        onChange={(e) =>
                          setFilterRating(parseFloat(e.target.value))
                        }
                      />
                    </Col>
                    <Col lg={6}>
                      <div className={"filter-group-title"}>
                        <label htmlFor="safety">Safety Rating (1 to 5)</label>
                      </div>
                      <input
                        type="range"
                        min="1"
                        max="5"
                        value={filterSafety}
                        step="0.1"
                        name="safety"
                        id="safety"
                        onChange={(e) =>
                          setFilterSafety(parseFloat(e.target.value))
                        }
                      />
                    </Col>
                  </Row>
                </div>

                <div className={"filter-group"}>
                  <div className={"filter-group-title"}>
                    <label htmlFor="covid-19">COVID-19 Levels (per 100k)</label>
                  </div>
                  <Row>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="lowest-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newCov = [...filterCOVID19];
                            newCov.push(100);
                            setFilterCOVID19(newCov);
                          } else {
                            var oldCov = [...filterCOVID19];
                            setFilterCOVID19(
                              oldCov.filter((item) => item !== 100)
                            );
                          }
                        }}
                        checked={filterCOVID19.includes(100)}
                      />
                      <label htmlFor="lowest-city" className={"checkbox-label"}>
                        1 to 100
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="low-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newCov = [...filterCOVID19];
                            newCov.push(500);
                            setFilterCOVID19(newCov);
                          } else {
                            var oldCov = [...filterCOVID19];
                            setFilterCOVID19(
                              oldCov.filter((item) => item !== 500)
                            );
                          }
                        }}
                        checked={filterCOVID19.includes(500)}
                      />
                      <label htmlFor="low-city" className={"checkbox-label"}>
                        101 to 500
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="low-med-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newCov = [...filterCOVID19];
                            newCov.push(1_000);
                            setFilterCOVID19(newCov);
                          } else {
                            var oldCov = [...filterCOVID19];
                            setFilterCOVID19(
                              oldCov.filter((item) => item !== 1_000)
                            );
                          }
                        }}
                        checked={filterCOVID19.includes(1_000)}
                      />
                      <label
                        htmlFor="low-med-city"
                        className={"checkbox-label"}
                      >
                        500 to 1,000
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="med-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newCov = [...filterCOVID19];
                            newCov.push(10_000);
                            setFilterCOVID19(newCov);
                          } else {
                            var oldCov = [...filterCOVID19];
                            setFilterCOVID19(
                              oldCov.filter((item) => item !== 10_000)
                            );
                          }
                        }}
                        checked={filterCOVID19.includes(10_000)}
                      />
                      <label htmlFor="med-city" className={"checkbox-label"}>
                        1,001 to 10,000
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="high-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newCov = [...filterCOVID19];
                            newCov.push(50_000);
                            setFilterCOVID19(newCov);
                          } else {
                            var oldCov = [...filterCOVID19];
                            setFilterCOVID19(
                              oldCov.filter((item) => item !== 50_000)
                            );
                          }
                        }}
                        checked={filterCOVID19.includes(50_000)}
                      />
                      <label htmlFor="high-city" className={"checkbox-label"}>
                        <span style={{ fontSize: "93%" }}>
                          10,001 to 50,000
                        </span>
                      </label>
                    </Col>
                    <Col lg={4}>
                      <input
                        type="checkbox"
                        id="highest-city"
                        onChange={(e) => {
                          if (e.target.checked) {
                            var newCov = [...filterCOVID19];
                            newCov.push(10_000_000);
                            setFilterCOVID19(newCov);
                          } else {
                            var oldCov = [...filterCOVID19];
                            setFilterCOVID19(
                              oldCov.filter((item) => item !== 10_000_000)
                            );
                          }
                        }}
                        checked={filterCOVID19.includes(10_000_000)}
                      />
                      <label
                        htmlFor="highest-city"
                        className={"checkbox-label"}
                      >
                        50,000+
                      </label>
                    </Col>
                  </Row>
                </div>
              </div>
            </Filter>
          </Col>
        </Row>
        <Row style={{ marginBottom: "50px" }}>
          <Col>
            <SearchGrid
              data={cities}
              searchQuery={filterName}
              pageNum={currPage}
              totalInstances={numCities}
              pagination={
                <ModelPagination
                  activePage={currPage}
                  firstPage={1}
                  lastPage={Math.ceil(
                    (numCities * 1.0) / MAX_INSTANCES_PER_PAGE
                  )}
                  updatePage={(p) => {
                    setCurrPage(p);
                  }}
                />
              }
            />
          </Col>
        </Row>
      </Col>
    </Container>
  );
}

export default CitySearch;
