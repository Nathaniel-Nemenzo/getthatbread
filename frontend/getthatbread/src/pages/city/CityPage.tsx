import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Carousel,
  CarouselItem,
} from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

import Map from "../../components/maps/Map";
import SearchCard from "../../components/search/searchgrid/SearchCard";
import { CompanyInterface, JobInterface } from "../../interfaces";
import {
  checkUnknowns,
  getLogoLink,
  formatNumbers,
  getStarRating,
} from "../../library/Functions";

import "../../index.css";

function getDetails(myDetails: string[]) {
  return myDetails.map((benefit) => {
    return (
      <Col lg={1} className={"benefits-column"}>
        <img alt={benefit} src={benefit} className={"benefits-group"} />
      </Col>
    );
  });
}

function getPhotos(myPhotos: string[]) {
  return myPhotos.map((photo) => {
    return (
      <CarouselItem>
        <img className="d-block carousel-img" src={photo} alt={photo} />
      </CarouselItem>
    );
  });
}

function CityPage() {
  const { cityID } = useParams();
  const [city, setCity] = useState({
    id: cityID,
    name: "",
    state: "",
    state_code: "",
    country: "",
    country_code: "",
    image: "",
    population: "",
    latitude: undefined,
    longitude: undefined,
    elevation: 0,
    rating: 0,
    safety: 0,
    covid: 0,
    budget: 0,
    details: [] as string[],
    photos: [] as string[],

    // urls
    airbnb_url: "",
    getyourguide_url: "",
    kayak_car_rental_url: "",
    kayak_lodging_url: "",
    roadgoat_url: "",
    walk_score_url: "",
  });

  const [companies, setCompanies] = useState([] as CompanyInterface[]);
  const [jobs, setJobs] = useState([] as JobInterface[]);

  useEffect(() => {
    axios
      .get(`https://api.getthatbread.me/api/cities/${cityID}`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        const data = response["data"];

        const cityInstance = {
          id: cityID,
          name: data["cityName"],
          state: checkUnknowns(data["state"], "Unknown"),
          state_code: data["stateCode"],
          country: checkUnknowns(data["country"], "Unknown"),
          country_code: data["countryCode"],
          image: data["highImage"],
          population: formatNumbers(data["population"]),
          latitude: data["latitude"],
          longitude: data["longitude"],
          elevation: data["elevationMeters"],
          rating: data["average_rating"],
          safety: data["safety"],
          covid: data["covid"],
          budget: data["budget"],
          details: data["known_for"]["data"].map(
            (a: { id: Number; url: String }) => a.url
          ),
          photos: data["photos"]["data"],

          // urls
          airbnb_url: data["airbnb_url"],
          getyourguide_url: data["getyourguide_url"],
          kayak_car_rental_url: data["kayak_car_rental_url"],
          kayak_lodging_url: data["kayak_lodging_url"],
          roadgoat_url: data["roadgoat_url"],
          walk_score_url: data["walk_score_url"],
        };

        setCity(cityInstance);

        /* We have the city! Now let's get the associated companies. */
        cityInstance.name =
          cityInstance.name === "New York City"
            ? "New York"
            : cityInstance.name;
        var compParams = { hq_location: cityInstance.name };
        console.log(cityInstance.name);
        axios
          .get(`https://api.getthatbread.me/api/companies`, {
            headers: {
              "Content-Type": "application/json",
            },
            params: compParams,
          })
          .then((response) => {
            const data = response["data"]["data"];
            const companies: CompanyInterface[] = [];

            for (let comp of data) {
              let compInstance: CompanyInterface = {
                type: "company",
                id: comp["companyId"],
                title: checkUnknowns(comp["companyName"], "Unknown").replace(
                  /\..*$/,
                  ""
                ),
                icon: `https://logo.clearbit.com/${getLogoLink(
                  comp["companyName"]
                )}.com`,
                subtitle: checkUnknowns(comp["hqLocation"], "Unknown"),
                keywords: [checkUnknowns(comp["sector"], "Unknown")],
                additional_info: [checkUnknowns(comp["employees"], "Unknown")],
                description: [
                  `Number of employees: ${checkUnknowns(
                    comp["employees"],
                    "Unknown"
                  )}`,
                  `Rating: ${comp["rating"]}`,
                ],

                // for formatting issues
                card_size: 12,
              };

              companies.push(compInstance);
            }

            setCompanies(companies);
          })
          .catch((error) => console.log(error));

        /* We have the city! Now let's get the associated jobs. */
        var cityParams = { location: cityInstance.name };
        axios
          .get(`https://api.getthatbread.me/api/jobs`, {
            headers: {
              "Content-Type": "application/json",
            },
            params: cityParams,
          })
          .then((response) => {
            const data = response["data"]["data"];
            const jobs: JobInterface[] = [];

            for (let job of data) {
              let jobInstance: JobInterface = {
                type: "job",
                id: job["jobId"],
                title: job["jobTitle"],
                icon: `https://logo.clearbit.com/${getLogoLink(
                  job["companyName"]
                )}.com`,
                subtitle: `${job["companyName"]} | ${
                  job["location"]
                } | ${checkUnknowns(job["salary"], "Unknown")}`,
                keywords: [checkUnknowns(job["jobType"], "Unknown")],
                additional_info: [],
                description: [],

                // for formatting issues
                card_size: 12,
              };
              jobs.push(jobInstance);
            }
            setJobs(jobs);
          })
          .catch((error) => console.log(error));
      })
      .catch((error) => console.log(error));
  }, [cityID]);

  return (
    <Container>
      <Row className="mt-4" style={{ marginBottom: "30px" }}>
        <Col lg={8}>
          <Row>
            <Card>
              <Card.Body>
                <Row>
                  <Col lg={2} className="city-pic">
                    <img alt="city" src={city?.image} width={"100%"} />
                  </Col>
                  <Col lg={10}>
                    <Card.Title>{city?.name + ", " + city?.state}</Card.Title>
                    <Card.Subtitle>{city?.country}</Card.Subtitle>
                  </Col>
                </Row>

                {/* Specific details about city */}
                <Row style={{ marginTop: "12px" }}>
                  <Col lg={2}>
                    <h4 className={"instance-h4"}>Population</h4>
                    {city?.population}
                  </Col>
                  <Col lg={2}>
                    <h4 className={"instance-h4"}>Elevation</h4>
                    {city?.elevation} meters
                  </Col>
                  <Col lg={3}>
                    <h4 className={"instance-h4"}>COVID-19 Levels</h4>
                    {Math.trunc(city?.covid)} per 100k people
                  </Col>
                  <Col lg={9}>
                    <h4 className={"instance-h4"}>Known For</h4>
                    <Row>{getDetails(city?.details ?? [])}</Row>
                  </Col>
                </Row>

                {/* Display ratings */}
                <Row>
                  <Col>
                    <Row>
                      <Col>
                        <h4 className={"instance-h4"}>Ratings</h4>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={4}>
                        Overall: {getStarRating(city?.rating, 5)} (
                        {city?.rating.toFixed(2)})
                      </Col>
                      <Col lg={4}>
                        Safety: {getStarRating(city?.safety, 5)} ({city?.safety}
                        )
                      </Col>
                      <Col lg={4}>
                        Budget: {getStarRating(city?.budget, 8)} ({city?.budget}
                        )
                      </Col>
                    </Row>
                  </Col>
                </Row>

                {/* Display vacation URLS */}
                <Row>
                  <Col>
                    <Row>
                      <Col>
                        <h4 className={"instance-h4"}>Learn More</h4>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <a href={city?.airbnb_url} className={"card-links"}>
                          Airbnb
                        </a>
                      </Col>
                      <Col>
                        <a
                          href={city?.kayak_car_rental_url}
                          className={"card-links"}
                        >
                          Car Rentals
                        </a>
                      </Col>
                      <Col>
                        <a
                          href={city?.kayak_lodging_url}
                          className={"card-links"}
                        >
                          Lodging
                        </a>
                      </Col>
                      <Col>
                        <a href={city?.roadgoat_url} className={"card-links"}>
                          Road Goat
                        </a>
                      </Col>
                      <Col>
                        <a href={city?.walk_score_url} className={"card-links"}>
                          Walk Score
                        </a>
                      </Col>
                    </Row>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <h3 className={"instance-h3"}>Photos</h3>
                    <Carousel>{getPhotos(city?.photos)}</Carousel>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Row>

          <Row className={"mt-4"}>
            <Card>
              <Card.Body>
                {/* Map of the city */}
                <Row>
                  <Col>
                    {city?.latitude === undefined ||
                    city?.longitude === undefined ? (
                      <span>Location cannot be found</span>
                    ) : (
                      <Map
                        latitude={city?.latitude}
                        longitude={city?.longitude}
                      />
                    )}
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Row>
        </Col>
        <Col lg={4}>
          <Row className={"half-height"}>
            <Col>
              <Card className={"half-height"}>
                <Card.Body className={"card-scroll-overflow"}>
                  <Card.Title>{`Companies in ${city?.name}`}</Card.Title>
                  <Card.Text>
                    {companies.map((item) => {
                      return <SearchCard data={item} />;
                    })}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row className={"half-height mt-4"}>
            <Col>
              <Card className={"half-height"}>
                <Card.Body className={"card-scroll-overflow"}>
                  <Card.Title>{`Jobs in ${city?.name}`}</Card.Title>
                  <Card.Text>
                    {jobs.map((item) => {
                      return <SearchCard data={item} />;
                    })}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default CityPage;
