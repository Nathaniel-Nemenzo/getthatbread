import React, { useEffect, useState } from "react";
import { Container, Row, Col, Spinner } from "react-bootstrap";
import PartyChart from "./components/PartyChart";
import PoliticianChart from "./components/PoliticianChart";
import axios from "axios";
import MedianIncome from "./components/MedianIncome";

function ProviderVisualizations() {
  const [districts, setDistricts] = useState([]);

  useEffect(() => {
    axios
      .get("https://api.electrends.me/districts?per_page=1000", {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        const data = response["data"];
        setDistricts(data);
      });
  });

  return (
    <Container style={{ marginTop: "36px", marginBottom: "36px" }}>
      <Row className={"justify-content-md-center"}>
        <Col md={10} lg={10}>
          <a
            href="https://www.electrends.me/"
            target="_blank"
            className={"card-links"} rel="noreferrer"
          >
            <h1 className={"main-title"}>Provider Visualizations</h1>
          </a>
        </Col>
      </Row>
      <Row className={"justify-content-md-center"}>
        <Col md={10} lg={10}>
          <Row>
            <Col>
              <h1>Median Income per District</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            {districts.length === 0 ? (
              <Spinner animation="border" />
            ) : (
              <MedianIncome data={districts} />
            )}
          </Row>
        </Col>
      </Row>
      <Row className={"justify-content-md-center"}>
        <Col md={10} lg={10}>
          <Row>
            <Col>
              <h1>Party Breakdown of Elections</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            <PartyChart />
          </Row>
        </Col>
      </Row>
      <Row className={"justify-content-md-center"}>
        <Col md={10} lg={10}>
          <Row>
            <Col>
              <h1>Politicians Who've Been in Office</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            <PoliticianChart />
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default ProviderVisualizations;
