import React, { useEffect, useState } from "react";
import { Container, Row, Spinner } from "react-bootstrap";
import {
  BarChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Bar,
} from "recharts";

const API_URL = "https://api.electrends.me/elections?per_page=1000";

const PartyChart = () => {
  useEffect(() => {
    fetchData();
  }, []);

  const [data, setData] = useState([]);

  const fetchData = async () => {
    try {
      const res = await fetch(API_URL);
      const data = await res.json();
      setData(data.elections);
    } catch (err) {
      console.log("API ERROR", err);
      setData([]);
    }
  };

  function getValues() {
    var values = [];
    var candidates = {};
    var winners = {};

    for (var i = 0; i < data.length; i++) {
      var party = data[i]["winning_party"];
      if (party in winners) {
        winners[party]++;
      } else {
        winners[party] = 1;
      }
      var politician = data[i]["politicians"];
      for (var j = 0; j < politician.length; j++) {
        party = politician[j]["party"];
        if (party in candidates) {
          candidates[party]++;
        } else {
          candidates[party] = 1;
        }
      }
    }

    for (var item in candidates) {
      if (item in winners) {
      } else {
        winners[item] = 0;
      }
      values.push({
        party: item,
        candidates: candidates[item],
        "winning candidates": winners[item],
      });
    }
    return values;
  }

  return data.length === 0 ? (
    <Spinner animation="border" />
  ) : (
    <Container>
      <Row className={"justify-content-md-center"}>
        <BarChart
          width={800}
          height={600}
          data={getValues()}
          margin={{ top: 10, right: 50, left: 50, bottom: 50 }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="party"></XAxis>
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="candidates" fill="#8884d8" />
          <Bar dataKey="winning candidates" fill="#82ca9d" />
        </BarChart>
      </Row>
    </Container>
  );
};

export default PartyChart;
