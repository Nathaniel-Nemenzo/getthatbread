import React from "react";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import { Container } from "react-bootstrap";

function PopulationCovidVisualization(props) {
  return (
    <Container>
      <ScatterChart
        width={1000}
        height={800}
        margin={{
          top: 30,
          bottom: 30,
          right: 30,
          left: 30,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="population" name="population" unit="" reversed />
        <YAxis dataKey="covid" name="covid" unit="" />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
        <Legend />
        <Scatter
          name="covid (people per 100k people)"
          data={props.data["data"]}
          fill="#8884d8"
        />
      </ScatterChart>
    </Container>
  );
}

export default PopulationCovidVisualization;
