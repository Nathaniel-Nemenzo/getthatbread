import React from "react";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import { Container } from "react-bootstrap";

function MedianIncome(props) {
  return (
    <Container>
      <ScatterChart
        width={1000}
        height={800}
        margin={{
          top: 30,
          bottom: 30,
          right: 30,
          left: 30,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" name="Disctricts" unit="" reversed />
        <YAxis dataKey="median_income" name="median_income" unit="" />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
        <Legend />
        <Scatter
          name="Median Income per District"
          data={props.data["districts"]}
          fill="#8884d8"
        />
      </ScatterChart>
    </Container>
  );
}

export default MedianIncome;
