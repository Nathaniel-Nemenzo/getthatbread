import React, { useEffect, useState } from "react";
import { Container, Row, Spinner } from "react-bootstrap";
import { RadialBarChart, RadialBar, Tooltip, Legend } from "recharts";

const API_URL = "https://api.electrends.me/politicians?per_page=1000";

const PoliticianChart = () => {
  useEffect(() => {
    fetchData();
  }, []);

  const [data, setData] = useState([]);
  const interval = ["0", "1-2", "3-5", "6-8", "9-11", "12-14", "15+"];
  const color = [
    "#8784d8",
    "#84a6ed",
    "#8ed1e1",
    "#82da9d",
    "#a2de6c",
    "#d0dd57",
    "#ffa658",
  ];

  const fetchData = async () => {
    try {
      const res = await fetch(API_URL);
      const data = await res.json();
      setData(data.politicians);
    } catch (err) {
      console.log("API ERROR", err);
      setData([]);
    }
  };

  function getValues() {
    var values = [];
    var politicians = {};

    for (var i = 0; i < data.length; i++) {
      var years = data[i]["years_in_office"];
      if (years >= 15) {
        var name = "15+";
      } else if (years === 0) {
        name = "0";
      } else {
        name = interval[Math.floor(years / 3) + 1];
      }
      console.log("years", years);
      console.log("name", name);
      if (name in politicians) {
        politicians[name]++;
      } else {
        politicians[name] = 1;
      }
    }
    var j = 0;
    for (var k = 0; k < interval.length; k++) {
      var item = interval[k];
      values.push({ name: item, people: politicians[item], fill: color[j++] });
    }

    return values;
  }

  return data.length === 0 ? (
    <Spinner animation="border" />
  ) : (
    <Container>
      <Row className={"justify-content-md-center"}>
        <RadialBarChart
          width={800}
          height={500}
          cx={250}
          cy={250}
          innerRadius={20}
          outerRadius={240}
          barSize={15}
          data={getValues()}
        >
          <RadialBar
            minAngle={15}
            label={{ fill: "#666", position: "outsideStart" }}
            background
            clockWise={true}
            dataKey="people"
          />
          <Legend
            iconSize={10}
            width={120}
            height={140}
            layout="vertical"
            verticalAlign="middle"
            align="right"
          />
          <Tooltip />
        </RadialBarChart>
      </Row>
    </Container>
  );
};

export default PoliticianChart;
