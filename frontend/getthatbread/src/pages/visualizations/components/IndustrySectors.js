import React, { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { PieChart, Pie, Cell, Tooltip } from "recharts";

function IndustrySectors(props) {
  /* Define proportions (async loading) */
  const [proportions, setProportions] = useState([]);
  const COLORS = [
    "#FF9AA2",
    "#FFB7B2",
    "#FFDAC1",
    "#E2F0CB",
    "#B5EAD7",
    "#C7CEEA",
  ];

  useEffect(() => {
    const p = {};

    /* create json */
    for (const company of props.data["data"]) {
      const sector = company["sector"];
      if (p.hasOwnProperty(sector)) {
        p[sector] += 1;
      } else {
        p[sector] = 1;
      }
    }

    /* create array */
    const data = [];
    Object.keys(p).forEach((key) => {
      data.push({
        name: key,
        value: p[key],
      });
    });

    console.log(data);
    setProportions(data);
  }, [props.data]);

  return (
    <Container>
      <PieChart
        width={1000}
        height={800}
        margin={{
          top: 30,
          bottom: 30,
          right: 30,
          left: 30,
        }}
      >
        <Pie
          data={proportions}
          dataKey="value"
          nameKey="name"
          fill="#8884d8"
          // innerRadius={60}
          // outerRadius={80}
        >
          {proportions.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
      </PieChart>
    </Container>
  );
}

export default IndustrySectors;
