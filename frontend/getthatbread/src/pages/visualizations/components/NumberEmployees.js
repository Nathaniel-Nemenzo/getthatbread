import React, { useState, useEffect } from "react";
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip } from "recharts";
import { Container } from "react-bootstrap";

function NumberEmployees(props) {
  /* Define proportions (async loading) */
  const [proportions, setProportions] = useState([]);

  useEffect(() => {
    const p = {};
    for (const company of props.data["data"]) {
      const numEmployees = company["employees"];
      if (p.hasOwnProperty(numEmployees)) {
        p[numEmployees] += 1;
      } else {
        p[numEmployees] = 1;
      }
    }
    var data = [];
    Object.keys(p).forEach((key) => {
      if (key !== "nan") {
        data.push({
          name: key,
          value: p[key],
        });
      }
    });

    // sorting the columns by company size
    data.sort((a, b) => {
      let aStr = "" + a.value;
      let bStr = "" + b.value;
      let aNum = parseInt(aStr.split(" ")[0]);
      let bNum = parseInt(bStr.split(" ")[0]);
      return aNum - bNum;
    });

    setProportions(data);
  }, [props.data]);

  return (
    <Container>
      <BarChart
        width={1100}
        height={800}
        margin={{
          top: 30,
          bottom: 30,
          right: 30,
          left: 30,
        }}
        data={proportions}
      >
        <Bar dataKey="value" fill="#8884d8" />
        <CartesianGrid stroke="#ccc" />
        <XAxis dataKey="name" />
        <YAxis dataKey="value" />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
      </BarChart>
    </Container>
  );
}

export default NumberEmployees;
