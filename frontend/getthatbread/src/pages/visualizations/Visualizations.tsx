import React, { useEffect, useState } from "react";
import { Container, Row, Col, Spinner } from "react-bootstrap";
import axios from "axios";

// imports
import IndustrySectors from "./components/IndustrySectors";
import PopulationVsCovid from "./components/PopulationVsCovid";
import NumberEmployees from "./components/NumberEmployees";

function Visualizations() {
  /* Define states */
  const [cities, setCities] = useState([]);
  const [companies, setCompanies] = useState([]);

  /* Make API calls (make sure to use a different useEffect() call for different API calls */
  useEffect(() => {
    axios
      .get(`https://api.getthatbread.me/api/cities`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        const data = response["data"];
        setCities(data);
      });
  });

  useEffect(() => {
    axios
      .get(`https://api.getthatbread.me/api/companies`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        const data = response["data"];
        setCompanies(data);
      })
      .catch((error) => console.log(error));
  });

  return (
    <Container style={{ marginTop: "36px", marginBottom: "36px" }}>
      <Row className={"justify-content-md-center"}>
        <Col md={10} lg={10}>
          <h1 className={"main-title"}>Our Visualizations</h1>
        </Col>
      </Row>
      <Row className={"justify-content-md-center"}>
        <Col md={10} lg={10}>
          <Row>
            <Col>
              <h1>Population Against COVID-19 Levels</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            {cities.length === 0 ? (
              <Spinner animation="border" />
            ) : (
              <PopulationVsCovid data={cities} />
            )}
          </Row>
        </Col>
      </Row>
      <Row className={"justify-content-md-center"}>
        <Col md={10} lg={10}>
          <Row>
            <Col>
              <h1>Company Sectors Breakdown</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            {companies.length === 0 ? (
              <Spinner animation="border" />
            ) : (
              <IndustrySectors data={companies} />
            )}
          </Row>
        </Col>
      </Row>
      <Row className={"justify-content-md-center"}>
        <Col md={10} lg={10}>
          <Row>
            <Col>
              <h1>Number of Employees for Company Sizes</h1>
            </Col>
          </Row>
          <Row className={"justify-content-md-center"}>
            {companies.length === 0 ? (
              <Spinner animation="border" />
            ) : (
              <NumberEmployees data={companies} />
            )}
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default Visualizations;
