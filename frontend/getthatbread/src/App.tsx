import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import MyNavbar from "./components/navbar/navbar.jsx";
import { ParallaxProvider } from "react-scroll-parallax";

// importing pages
import AboutPage from "./pages/about/AboutPage";
import CitySearch from "./pages/city/CitySearch";
import CityPage from "./pages/city/CityPage";
import CompanySearch from "./pages/company/CompanySearch";
import CompanyPage from "./pages/company/CompanyPage";
import JobSearch from "./pages/job/JobSearch";
import JobPage from "./pages/job/JobPage";
import ProviderVisualizations from "./pages/visualizations/ProviderVisualizations";
import SplashPage from "./pages/home/SplashPage";
import SearchPage from "./pages/search/SearchPage";
import Visualizations from "./pages/visualizations/Visualizations";

function App() {
  return (
    <>
      <ParallaxProvider>
        <Router>
          <MyNavbar />
          <Routes>
            <Route path="/" element={<SplashPage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/search:searchID" element={<SearchPage />} />

            <Route path="/job" element={<JobSearch />} />
            <Route path="/job/:jobID" element={<JobPage />} />

            <Route path="/company" element={<CompanySearch />} />
            <Route path="/company/:companyID" element={<CompanyPage />} />

            <Route path="/city" element={<CitySearch />} />
            <Route path="/city/:cityID" element={<CityPage />} />

            <Route path="/visuals" element={<Visualizations />} />
            <Route path="provider" element={<ProviderVisualizations />} />
          </Routes>
        </Router>
      </ParallaxProvider>
    </>
  );
}

export default App;
