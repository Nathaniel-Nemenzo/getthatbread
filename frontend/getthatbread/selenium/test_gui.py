# based on: https://gitlab.com/10am-group-8/adopt-a-pet/-/blob/main/front-end/selenium/gui_test.py
import sys
import time
import pytest

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = None
wait = None
local = False # set to FALSE when pushing to gitlab

url = "https://www.getthatbread.me/"

def setup_module():
    print("beginning setup for test_gui module")

    # allow gitlab ci/cd to run selenium tests
    global driver, wait
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("window-size=1200x600")
    if local:
        driver = webdriver.Chrome(service = Service(ChromeDriverManager().install()), chrome_options = chrome_options)
    else:
        driver = Remote(
            "http://selenium__standalone-chrome:4444/wd/hub",
            desired_capabilities=chrome_options.to_capabilities(),
        )
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    return driver

def teardown_module():
    print("tearing down test_gui module")
    driver.quit()

'''
Basic Tests
'''
def test_title():
    print("starting test_title")
    assert driver.title == "GetThatBread"

'''
Navbar Tests
'''
def test_navbar_home():
    print("starting test_navbar_home")
    home = driver.find_elements(By.XPATH, "/html/body/div/nav/div/a")
    home[0].click()
    assert driver.current_url == url

def test_navbar_jobs():
    print("starting test_navbar_jobs")
    jobs = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[1]")
    jobs.click()
    assert driver.current_url == url + "job/"

def test_navbar_companies():
    print("starting test_navbar_companies")
    companies = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[2]")
    companies.click()
    assert driver.current_url == url + "company/"

def test_navbar_cities():
    print("starting test_navbar_cities")
    cities = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[3]")
    cities.click()
    assert driver.current_url == url + "city/"

def test_navbar_search():
    print("starting test_navbar_search")
    cities = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[4]")
    cities.click()
    assert driver.current_url == url + "search&/"

'''
About Page Tests
'''
def test_navbar_about():
    print("starting test_navbar_about")
    about = driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[7]")
    about.click()
    assert driver.current_url == url + "about/"

def test_about_page_title():
    print("starting test_about_page_title")
    driver.get(url + 'about/')
    title = driver.find_element(By.XPATH, "/html/body/div/div/div/div[1]/div/h1")
    assert title.text == "About Us"

def test_about_page_stats():
    print("starting test_about_page_stats")
    driver.get(url + 'about/')
    stats_title = driver.find_element(By.XPATH, "/html/body/div/div/div/div[4]/div/h1")
    assert stats_title.text == "Total Statistics"

def test_about_page_docs():
    print("starting test_about_page_docs")
    driver.get(url + 'about/')
    doc_title = driver.find_element(By.XPATH, "/html/body/div/div/div/div[6]")
    doc_title = doc_title.find_elements(By.TAG_NAME, "h1")
    assert doc_title[0].text == "Our Documentation"

def test_about_page_tech():
    print("starting test_about_page_tech")
    driver.get(url + 'about/')
    tech_title = driver.find_element(By.XPATH, "/html/body/div/div/div/div[8]")
    tech_title = tech_title.find_elements(By.TAG_NAME, "h1")
    assert tech_title[0].text == "Our Technologies"

def test_about_page_apis():
    print("starting test_about_page_apis")
    driver.get(url + 'about/')
    assert "Our APIs" in driver.page_source

'''
Search Page Tests
'''
def test_search_page_title():
    print("starting test_search_page_title")
    driver.get(url + 'search&/')
    assert "Start a new search." in driver.page_source

def test_search_page_models():
    print("starting test_search_page_models")
    driver.get(url + 'search&/')

    model_titles = driver.find_elements(By.TAG_NAME, "h3")
    driver.implicitly_wait(5)

    assert model_titles[0].text == "Jobs"
    assert model_titles[1].text == "Companies"
    assert model_titles[2].text == "Cities"

'''
Job Page Tests
'''
def test_job_page_filters():
    print("starting test_job_page_filters")
    driver.get(url + 'job/')

    driver.implicitly_wait(5)
    button = driver.find_elements(By.CLASS_NAME, "btn-outline-primary")
    button[0].click()

    # should open filters modal
    driver.implicitly_wait(10)
    assert "Filters" in driver.page_source

def test_job_page_card():
    print("starting test_job_page_card")
    driver.get(url + 'job/')

    driver.implicitly_wait(2)
    card = driver.find_elements(By.CLASS_NAME, "search-card")
    card[0].click()

    driver.implicitly_wait(5)
    assert driver.current_url != url + "job/"

def test_job_page_sort():
    print("starting test_job_page_sort")
    driver.get(url + 'job/')

    driver.implicitly_wait(2)
    sort_btn = driver.find_elements(By.XPATH, "/html/body/div/div/div/div[1]/div/div/div/div[1]/div/div[2]/div/button")
    sort_btn[0].click()

    option = driver.find_elements(By.CLASS_NAME, "dropdown-item")[0]
    assert option.text == "Name (A-Z)"
    option.click()

def test_job_page_search():
    print("starting test_job_page_search")
    driver.get(url + 'job/')

    driver.implicitly_wait(2)
    search_bar = driver.find_elements(By.XPATH, "/html/body/div/div/div/div[1]/div/div/div/div[2]/form/div/input")
    assert search_bar[0].get_attribute('placeholder') == "Search by job title"

'''
Company Page Tests
'''
def test_company_page_filters():
    print("starting test_company_page_filters")
    driver.get(url + 'company/')

    driver.implicitly_wait(5)
    button = driver.find_elements(By.CLASS_NAME, "btn-outline-primary")
    button[0].click()

    # should open filters modal
    driver.implicitly_wait(10)
    assert "Filters" in driver.page_source

def test_company_page_card():
    print("starting test_company_page_card")
    driver.get(url + 'company/')

    driver.implicitly_wait(2)
    card = driver.find_elements(By.CLASS_NAME, "search-card")
    card[0].click()

    driver.implicitly_wait(5)
    assert driver.current_url != url + "company/"

def test_company_page_sort():
    print("starting test_company_page_sort")
    driver.get(url + 'company/')

    driver.implicitly_wait(2)
    sort_btn = driver.find_elements(By.XPATH, "/html/body/div/div/div/div[1]/div/div/div/div[1]/div/div[2]/div/button")
    sort_btn[0].click()

    option = driver.find_elements(By.CLASS_NAME, "dropdown-item")[0]
    assert option.text == "Name (A-Z)"
    option.click()

def test_company_page_search():
    print("starting test_company_page_search")
    driver.get(url + 'company/')

    driver.implicitly_wait(2)
    search_bar = driver.find_elements(By.XPATH, "/html/body/div/div/div/div[1]/div/div/div/div[2]/form/div/input")
    assert search_bar[0].get_attribute('placeholder') == "Search for a company"

'''
City Page Tests
'''
def test_city_page_filters():
    print("starting test_city_page_filters")
    driver.get(url + 'city/')

    driver.implicitly_wait(5)
    button = driver.find_elements(By.CLASS_NAME, "btn-outline-primary")
    button[0].click()

    # should open filters modal
    driver.implicitly_wait(10)
    assert "Filters" in driver.page_source

def test_city_page_card():
    print("starting test_city_page_card")
    driver.get(url + 'city/')

    driver.implicitly_wait(2)
    card = driver.find_elements(By.CLASS_NAME, "search-card")
    card[0].click()

    driver.implicitly_wait(5)
    assert driver.current_url != url + "city/"

def test_city_page_sort():
    print("starting test_city_page_sort")
    driver.get(url + 'city/')

    driver.implicitly_wait(2)
    sort_btn = driver.find_elements(By.XPATH, "/html/body/div/div/div/div[1]/div/div/div/div[1]/div/div[2]/div/button")
    sort_btn[0].click()

    option = driver.find_elements(By.CLASS_NAME, "dropdown-item")[0]
    assert option.text == "Name (A-Z)"
    option.click()

def test_city_page_search():
    print("starting test_city_page_search")
    driver.get(url + 'city/')

    driver.implicitly_wait(2)
    search_bar = driver.find_elements(By.XPATH, "/html/body/div/div/div/div[1]/div/div/div/div[2]/form/div/input")
    assert search_bar[0].get_attribute('placeholder') == "Search for a city"