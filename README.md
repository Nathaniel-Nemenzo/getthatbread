# GetThatBread
## Names, EIDs, and Gitlab IDs
| Name                 | UTEID   | GitLab ID          |
| -------------------- | ------- | ------------------ |
| Abinith Thallam      | at39785 | @abinith.thallam   |
| Hamza Ali            | hia273  | @hamzaali1         |
| Lilly Tian           | lt23737 | @lillytian         |
| Nathaniel Nemenzo    | ncn393  | @Nathaniel-Nemenzo |
| Yifan Zhou           | yz24275 | @yifanzhou1        |

## Git SHA
Phase 1: 9852cb5b7fab1348ef78e9f717904d7d98d50ca2

Phase 2: 8607869e7e8615f934d82d585156e24efc556a8c

Phase 3: e744c47d0e88545fe15f48c5e67693dad478382e

Phase 4: ea698686cbec056c8833d5a90aaa978d6fad3ba0

## Project Leader
Phase 1: Nathaniel Nemenzo (ncn393)

Phase 2: Yifan Zhou (yz24275)

Phase 3: Lilly Tian (lt23737)

Phase 4: Abinith Thallam (at39785)

## Link to CI/CD Pipeline
[Pipeline](https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/pipelines)

## Link to Website
[getthatbread](https://www.getthatbread.me)

## Link to API
[getthatbread-api](https://api.getthatbread.me)

## Completion Time / Member
### Phase 1
| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Abinith Thallam      | 13        | 16     |
| Hamza Ali            | 11        | 15     |
| Lilly Tian           | 15        | 20     |
| Nathaniel Nemenzo    | 12        | 14     |
| Yifan Zhou           | 10        | 18     |

### Phase 2
| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Abinith Thallam      | 13        | 20     |
| Hamza Ali            | 15        | 22     |
| Lilly Tian           | 12        | 20     |
| Nathaniel Nemenzo    | 20        | 30     |
| Yifan Zhou           | 48        | 48     |

### Phase 3
| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Abinith Thallam      | 10        | 10     |
| Hamza Ali            | 05        | 10     |
| Lilly Tian           | 30        | 45     |
| Nathaniel Nemenzo    | 25        | 30     |
| Yifan Zhou           | 08        | 08     |

### Phase 4
| Name                 | Estimated | Actual |
| -------------------- | --------- | ------ |
| Abinith Thallam      | 05        | 04     |
| Hamza Ali            | 02        | 02     |
| Lilly Tian           | 03        | 03     |
| Nathaniel Nemenzo    | 04        | 03     |
| Yifan Zhou           | 05        | 05     |

## Comments
Our provider's API seems to be down lately or times out whenever we try to make an API request, so we're not sure if the TAs will be able to view our provider visualizations.
